# Seize - Webmachine Extension For Hyper

[![pipeline status](https://gitlab.com/petejohanson/seize/badges/master/pipeline.svg)](https://gitlab.com/petejohanson/seize/commits/master)
[![codecov](https://codecov.io/gl/seize/seize/branch/master/graph/badge.svg)](https://codecov.io/gl/seize/seize)

An implementation (WIP) of the original Erlang [Webmachine](https://github.com/webmachine/webmachine/) for Rust. Seize builds on top of [Hyper](https://hyper.rs/),
allowing the creation of service functions from Webmachine resources.

# Getting Started

TODO:

# References

1. TODO: Link to docs
1. Link to Webmachine flow diagram.
