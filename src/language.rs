use core::convert::TryFrom;
use std::fmt::Display;
use std::iter;
use std::str;

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while, take_while_m_n},
    character::is_alphabetic,
    combinator::{all_consuming, map, map_parser, opt},
    error::ParseError,
    multi::many_m_n,
    sequence::{preceded, tuple},
    AsChar, Compare, IResult, InputIter, InputLength, InputTake, InputTakeAtPosition,
};

fn subtag_segment_parser<'a, I: 'a, O: 'a, E: ParseError<I> + 'a, F: 'a>(
    f: &'a F,
) -> impl Fn(I) -> IResult<I, O, E> + 'a
where
    I: Clone
        + PartialEq
        + InputTakeAtPosition
        + InputLength
        + InputIter
        + InputTake
        + Compare<&'static [u8]>,
    <I as InputTakeAtPosition>::Item: AsChar + Clone,
    F: Fn(I) -> IResult<I, O, E>,
{
    preceded(tag(b"-" as &[u8]), subtag_parser(f))
}

fn subtag_parser<'a, I: 'a, O: 'a, E: ParseError<I> + 'a, F: 'a>(
    f: &'a F,
) -> impl Fn(I) -> IResult<I, O, E> + 'a
where
    I: Clone + PartialEq + InputTakeAtPosition + InputLength + InputIter + InputTake,
    <I as InputTakeAtPosition>::Item: AsChar + Clone,
    F: Fn(I) -> IResult<I, O, E>,
{
    map_parser(
        take_while(|c: <I as InputTakeAtPosition>::Item| c.is_alphanum()),
        all_consuming(f),
    )
}

fn from_utf8_unsafe(s: &[u8]) -> &str {
    str::from_utf8(s).expect("Parse has already validated bytes")
}

/**
 * @see https://tools.ietf.org/html/rfc4646#section-2.1
 */
fn language_tag(input: &[u8]) -> IResult<&[u8], LanguageTag> {
    map(
        tuple((
            alt((
                tuple((
                    map(
                        subtag_parser(&take_while_m_n(2, 3, is_alphabetic)),
                        from_utf8_unsafe,
                    ),
                    opt(many_m_n(
                        0,
                        3,
                        subtag_segment_parser(&map(
                            take_while_m_n(3, 3, is_alphabetic),
                            from_utf8_unsafe,
                        )),
                    )),
                )),
                subtag_parser(&map(
                    map(take_while_m_n(4, 4, is_alphabetic), from_utf8_unsafe),
                    |a| (a, None),
                )),
                subtag_parser(&map(
                    map(take_while_m_n(5, 8, is_alphabetic), from_utf8_unsafe),
                    |a| (a, None),
                )),
            )),
            opt(subtag_segment_parser(&map(
                take_while_m_n(4, 4, is_alphabetic),
                from_utf8_unsafe,
            ))),
            opt(subtag_segment_parser(&map(
                alt((
                    take_while_m_n(2, 2, is_alphabetic),
                    take_while_m_n(3, 3, is_alphabetic),
                )),
                from_utf8_unsafe,
            ))),
        )),
        |((language, exts), script, region)| {
            LanguageTag::new(language, exts.unwrap_or(vec![]), script, region, vec![])
        },
    )(input)
}

#[derive(Debug, PartialEq, Clone)]
pub struct LanguageTag<'a> {
    language: &'a str,
    extended_language: Vec<&'a str>,
    script: Option<&'a str>,
    region: Option<&'a str>,
    variants: Vec<&'a str>,
    // extensions: Vec<String>,
    // private_use: Vec<String>,
}

impl<'a> LanguageTag<'a> {
    pub fn new(
        language: &'a str,
        extended_language: Vec<&'a str>,
        script: Option<&'a str>,
        region: Option<&'a str>,
        variants: Vec<&'a str>,
    ) -> Self {
        LanguageTag {
            language,
            extended_language,
            script,
            region,
            variants,
        }
    }

    pub fn iter<'b>(&'b self) -> impl Iterator<Item = &&'a str> + 'b {
        // TODO: extensions and private use
        iter::once(Some(&self.language))
            .chain(self.extended_language.iter().map(Some))
            .chain(iter::once(self.script.as_ref()))
            .chain(iter::once(self.region.as_ref()))
            .chain(self.variants.iter().map(Some))
            .filter_map(|o| o)
    }
}

impl<'a> Display for LanguageTag<'a> {
    // TODO: Better performance without cloned strings?
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.iter().cloned().collect::<Vec<_>>().join("-"))
    }
}

impl<'a> TryFrom<&'a [u8]> for LanguageTag<'a> {
    type Error = String;

    fn try_from(value: &'a [u8]) -> Result<LanguageTag, String> {
        // TODO: Better Result handling!
        all_consuming(language_tag)(value)
            .map(|(_, parsed)| parsed)
            .map_err(|e| format!("{:?}", e))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn language_tag_from_legacy() {
        assert_eq!(
            LanguageTag::try_from(b"klingon" as &[u8]),
            Ok(LanguageTag::new("klingon", vec![], None, None, vec![]))
        );
    }

    #[test]
    fn language_tag_from_str_extended_subtags() {
        assert_eq!(
            LanguageTag::try_from(b"zh-gan" as &[u8]),
            Ok(LanguageTag::new("zh", vec!["gan"], None, None, vec![]))
        );
    }

    #[test]
    fn language_tag_from_str_no_script() {
        assert_eq!(
            LanguageTag::try_from(b"de-DE" as &[u8]),
            Ok(LanguageTag::new("de", vec![], None, Some("DE"), vec![]))
        );
    }

    #[test]
    fn language_tag_display() {
        assert_eq!(
            format!("{}", LanguageTag::try_from(b"en-Latn-GB" as &[u8]).unwrap()),
            "en-Latn-GB"
        );
    }
}
