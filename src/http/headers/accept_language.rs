use crate::http::headers::quality::weight;
use crate::http::list1;
use crate::language::LanguageTag;
use core::convert::TryFrom;
use std::cmp::Ordering;
use std::str;

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while_m_n},
    character::is_alphanumeric,
    combinator::{map, opt},
    multi::many0,
    sequence::{pair, preceded},
    IResult,
};

fn language_segment(input: &[u8]) -> IResult<&[u8], &str> {
    map(take_while_m_n(1, 8, is_alphanumeric), |s| {
        str::from_utf8(s).expect("Previous parser check should validate valid str")
    })(input)
}

fn language_range(input: &[u8]) -> IResult<&[u8], LanguageRange> {
    alt((
        map(tag(b"*"), |_| LanguageRange::AllLanguages),
        map(
            pair(
                language_segment,
                many0(preceded(tag(b"-"), language_segment)),
            ),
            |(first, rest)| {
                LanguageRange::Tag(vec![first].into_iter().chain(rest.into_iter()).collect())
            },
        ),
    ))(input)
}

pub fn accept_language(input: &[u8]) -> IResult<&[u8], AcceptLanguage> {
    map(
        list1(&pair(language_range, opt(weight))),
        AcceptLanguage::new,
    )(input)
}

#[derive(Debug, PartialEq, PartialOrd, Ord, Eq)]
pub enum LanguageRange<'a> {
    Tag(Vec<&'a str>),
    AllLanguages,
}

impl<'a> LanguageRange<'a> {
    pub fn is_match(&self, lang: &LanguageTag) -> bool {
        match self {
            LanguageRange::Tag(tags) => {
                // Ultimately, as long as there is a shared prefix, this is a match,
                // so zip up both sequences, take while matching, and verify we can
                // get a first item out of the iterator.
                tags.iter()
                    .zip(lang.iter())
                    .take_while(|(a, b)| a.eq_ignore_ascii_case(b))
                    .next()
                    .is_some()
            }
            LanguageRange::AllLanguages => true,
        }
    }
}

#[derive(Debug, PartialEq, PartialOrd)]
pub struct AcceptLanguage<'a> {
    languages: Vec<(LanguageRange<'a>, Option<f32>)>,
}

impl<'a> AcceptLanguage<'a> {
    pub fn new(mut languages: Vec<(LanguageRange<'a>, Option<f32>)>) -> Self {
        languages.sort_by(|(a_r, a_w), (b_r, b_w)| {
            a_r.cmp(&b_r)
                .then(a_w.partial_cmp(&b_w).unwrap_or(Ordering::Equal))
        });

        AcceptLanguage { languages }
    }

    pub fn filtered<'b, 'c, I: 'b>(&self, langs: I) -> Vec<&'b LanguageTag<'c>>
    where
        I: Iterator<Item = &'b LanguageTag<'c>>,
    {
        let mut filtered = langs
            .filter_map(|lt| {
                self.languages
                    .iter()
                    .filter(|(lr, _)| lr.is_match(lt))
                    .collect::<Vec<_>>()
                    .first()
                    .map(|(_, q)| (lt, q.unwrap_or(1.0)))
            })
            .collect::<Vec<_>>();

        filtered.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap_or(Ordering::Equal).reverse());

        filtered.into_iter().map(|(lt, _)| lt).collect()
    }
}

impl<'a> TryFrom<&'a [u8]> for AcceptLanguage<'a> {
    type Error = String;

    fn try_from(s: &'a [u8]) -> Result<Self, String> {
        // TODO: Better Result handling!
        nom::combinator::all_consuming(accept_language)(s)
            .map(|(_, parsed)| parsed)
            .map_err(|e| format!("{:?}", e))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_language_range_match_exact_match() {
        assert!(
            LanguageRange::Tag(vec!["de", "de"]).is_match(&LanguageTag::new(
                "de",
                vec![],
                Some("DE"),
                None,
                vec![""]
            ))
        );
    }

    #[test]
    fn test_language_range_all_match_language_tag() {
        assert!(LanguageRange::AllLanguages.is_match(&LanguageTag::new(
            "de",
            vec![],
            Some("DE"),
            None,
            vec![""]
        )));
    }

    #[test]
    fn test_accept_language_try_from_with_multiples() {
        let al = AcceptLanguage::new(vec![
            (LanguageRange::Tag(vec!["da"]), None),
            (LanguageRange::Tag(vec!["en", "gb"]), Some(0.5)),
            (LanguageRange::Tag(vec!["en"]), Some(0.2)),
            (LanguageRange::AllLanguages, Some(1.0)),
        ]);

        assert_eq!(
            AcceptLanguage::try_from(b"da, en-gb;q=0.5, en;q=0.2, *;q=1" as &[u8]),
            Ok(al)
        );
    }
}
