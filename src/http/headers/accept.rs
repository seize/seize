use core::convert::TryFrom;
use std::cmp::Ordering;

use crate::http::media_type::{MediaType, Parameter};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::space0,
    combinator::{map, opt, verify},
    multi::many0,
    sequence::{preceded, separated_pair, terminated, tuple},
    IResult,
};

fn media_range_type(input: &[u8]) -> IResult<&[u8], MediaRangeType> {
    alt((
        map(tag(b"*/*"), |_| MediaRangeType::AllTypes),
        map(
            terminated(super::super::token, tag(b"/*")),
            MediaRangeType::AllSubtypes,
        ),
        map(
            separated_pair(super::super::token, tag(b"/"), super::super::token),
            |(t, s)| MediaRangeType::MediaType(t, s),
        ),
    ))(input)
}

fn parameter_list_until_weight(
    input: &[u8],
) -> IResult<&[u8], Vec<super::super::media_type::Parameter>> {
    many0(preceded(
        tuple((space0, tag(b";"), space0)),
        verify(super::super::parameter, |p| p.name() != "q"),
    ))(input)
}

fn media_range(input: &[u8]) -> IResult<&[u8], MediaRange> {
    map(
        tuple((media_range_type, parameter_list_until_weight)),
        |(t, ps)| MediaRange::new(t, ps),
    )(input)
}

fn accept_ext(input: &[u8]) -> IResult<&[u8], AcceptExtensionParameter> {
    // TODO: quoted-string
    map(
        preceded(
            tuple((space0, tag(b";"), space0)),
            tuple((
                super::super::token,
                opt(preceded(tag(b"="), super::super::token)),
            )),
        ),
        |(n, ov)| AcceptExtensionParameter::new(n, ov),
    )(input)
}

fn accept_params(input: &[u8]) -> IResult<&[u8], AcceptParams> {
    map(
        tuple((super::quality::weight, many0(accept_ext))),
        |(w, exts)| AcceptParams::new(w, exts),
    )(input)
}

fn accept(input: &[u8]) -> IResult<&[u8], Accept> {
    map(
        super::super::list0(&tuple((media_range, opt(accept_params)))),
        Accept::new,
    )(input)
}

#[derive(Debug, PartialEq, PartialOrd, Ord, Eq)]
pub enum MediaRangeType<'a> {
    // Order here is important, more specific range match prioritizes higher!
    MediaType(&'a str, &'a str),
    AllSubtypes(&'a str),
    AllTypes,
}

#[derive(Debug, PartialEq)]
pub struct MediaRange<'a> {
    r#type: MediaRangeType<'a>,
    parameters: Vec<Parameter<'a>>,
}

impl<'a> MediaRange<'a> {
    fn new(r#type: MediaRangeType<'a>, parameters: Vec<Parameter<'a>>) -> Self {
        MediaRange { r#type, parameters }
    }

    pub fn match_data(&self, mt: &MediaType) -> Option<(&MediaRangeType, usize)> {
        match &self.r#type {
            MediaRangeType::AllTypes => Some(&self.r#type),
            MediaRangeType::AllSubtypes(t) if *t == mt.type_() => Some(&self.r#type),
            MediaRangeType::MediaType(t, s) if *t == mt.type_() && *s == mt.subtype() => {
                Some(&self.r#type)
            }
            _ => None,
        }
        .and_then(|t| self.matching_parameters(mt.parameters()).map(|p| (t, p)))
    }

    fn matching_parameters(&self, b_params: &[Parameter]) -> Option<usize> {
        let (matches, failed): (Vec<_>, Vec<_>) = b_params
            .iter()
            .map(|bp| {
                (
                    bp,
                    self.parameters
                        .iter()
                        .filter(|sp| sp.name() == bp.name())
                        .collect::<Vec<_>>(),
                )
            })
            .partition(|(bp, name_matches)| name_matches.iter().any(|sp| sp.value() == bp.value()));

        if failed.len() > 0 {
            None
        } else {
            Some(matches.len())
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct AcceptParams<'a> {
    weight: f32,
    extensions: Vec<AcceptExtensionParameter<'a>>,
}

impl<'a> AcceptParams<'a> {
    fn new(weight: f32, extensions: Vec<AcceptExtensionParameter<'a>>) -> Self {
        AcceptParams { weight, extensions }
    }
}

#[derive(Debug, PartialEq)]
pub struct AcceptExtensionParameter<'a> {
    name: &'a str,
    value: Option<&'a str>,
}

impl<'a> AcceptExtensionParameter<'a> {
    fn new(name: &'a str, value: Option<&'a str>) -> Self {
        AcceptExtensionParameter { name, value }
    }
}

#[derive(Debug, PartialEq)]
pub struct Accept<'a> {
    media_ranges: Vec<(MediaRange<'a>, Option<AcceptParams<'a>>)>,
}

impl<'a> Accept<'a> {
    pub fn new(media_ranges: Vec<(MediaRange<'a>, Option<AcceptParams<'a>>)>) -> Self {
        Accept { media_ranges }
    }

    pub fn media_ranges(&self) -> &Vec<(MediaRange<'_>, Option<AcceptParams<'_>>)> {
        &self.media_ranges
    }

    pub fn best_match<'b, 'c, I: 'b>(&self, iter: I) -> Option<&'b MediaType<'c>>
    where
        I: Iterator<Item = &'b MediaType<'c>> + std::fmt::Debug,
    {
        // TODO: Use PartialEq for MediaRange?
        let mut qualified = iter
            .filter_map(|mt| {
                let mut candidates = self
                    .media_ranges()
                    .iter()
                    .filter_map(|(r, p)| r.match_data(mt).map(|md| (md, p)))
                    .collect::<Vec<_>>();

                // TODO: sort_by include parameters?
                candidates.sort_by(|a, b| (a.0).0.cmp(&(b.0).0));

                candidates
                    .first()
                    .map(|(_, &ref p)| (mt, p.as_ref().map(|p2| p2.weight).unwrap_or(0.001)))
            })
            .collect::<Vec<_>>();

        qualified.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap_or(Ordering::Equal));

        qualified.first().map(|(&ref mt, _weight)| mt)
    }
}

impl<'a> TryFrom<&'a [u8]> for Accept<'a> {
    type Error = String;

    fn try_from(value: &'a [u8]) -> Result<Accept<'a>, String> {
        // TODO: Better Result handling!
        nom::combinator::all_consuming(accept)(value)
            .map(|(_, parsed)| parsed)
            .map_err(|e| format!("{:?}", e))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all_types_media_range() {
        assert_eq!(
            media_range_type(b"*/*"),
            Ok((&[] as &[u8], MediaRangeType::AllTypes))
        );
    }

    #[test]
    fn test_all_subtypes_media_range() {
        assert_eq!(
            media_range_type(b"image/*"),
            Ok((&[] as &[u8], MediaRangeType::AllSubtypes("image")))
        );
    }

    #[test]
    fn test_specific_type_media_range() {
        assert_eq!(
            media_range_type(b"image/png"),
            Ok((&[] as &[u8], MediaRangeType::MediaType("image", "png")))
        );
    }

    #[test]
    fn test_media_range_with_parameters() {
        let parameter = Parameter::new("charset", "utf8");
        assert_eq!(
            media_range(b"*/*;charset=utf8"),
            Ok((
                &[] as &[u8],
                MediaRange::new(MediaRangeType::AllTypes, vec![parameter])
            ))
        );
    }

    #[test]
    fn test_accept_with_parameters() {
        let parameter = Parameter::new("charset", "utf8");
        let a = Accept::new(vec![(
            MediaRange::new(MediaRangeType::AllTypes, vec![parameter]),
            None,
        )]);

        assert_eq!(accept(b"*/*;charset=utf8"), Ok((&[] as &[u8], a)));
    }

    #[test]
    fn test_accept_with_weight_and_extensions() {
        let a = Accept::new(vec![(
            MediaRange::new(MediaRangeType::AllTypes, vec![]),
            Some(AcceptParams::new(
                0.2,
                vec![AcceptExtensionParameter::new("ext", Some("bob"))],
            )),
        )]);

        assert_eq!(accept(b"*/*;q=0.2;ext=bob"), Ok((&[] as &[u8], a)));
    }

    #[test]
    fn test_accept_with_multiples_and_empties_parameters() {
        let parameter = Parameter::new("charset", "utf8");
        let a = Accept::new(vec![
            (
                MediaRange::new(MediaRangeType::AllTypes, vec![parameter]),
                None,
            ),
            (
                MediaRange::new(MediaRangeType::MediaType("text", "plain"), vec![]),
                None,
            ),
        ]);

        assert_eq!(
            accept(b"*/*;charset=utf8   ,    , text/plain"),
            Ok((&[] as &[u8], a))
        );
    }

    #[test]
    fn test_accept_try_from_with_multiples_and_empties_parameters() {
        let parameter = Parameter::new("charset", "utf8");
        let a = Accept::new(vec![
            (
                MediaRange::new(MediaRangeType::AllTypes, vec![parameter]),
                None,
            ),
            (
                MediaRange::new(MediaRangeType::MediaType("text", "plain"), vec![]),
                None,
            ),
        ]);

        assert_eq!(
            Accept::try_from(b"*/*;charset=utf8   ,    , text/plain" as &[u8]),
            Ok(a)
        );
    }

    #[test]
    fn test_simple_wildcard_accept_best_match() {
        let a = Accept::new(vec![(
            MediaRange::new(MediaRangeType::AllTypes, vec![]),
            Some(AcceptParams::new(0.5, vec![])),
        )]);

        let text_plain = MediaType::new("text", "plain", vec![]);
        let candidates = vec![&text_plain];

        assert_eq!(a.best_match(candidates.into_iter()), Some(&text_plain));
    }

    #[test]
    fn test_specific_type_override_accept_best_match() {
        let a = Accept::new(vec![
            (
                MediaRange::new(MediaRangeType::AllTypes, vec![]),
                Some(AcceptParams::new(0.5, vec![])),
            ),
            (
                MediaRange::new(MediaRangeType::AllSubtypes("text"), vec![]),
                Some(AcceptParams::new(1.0, vec![])),
            ),
            (
                MediaRange::new(MediaRangeType::MediaType("text", "plain"), vec![]),
                Some(AcceptParams::new(0.001, vec![])),
            ),
            (
                MediaRange::new(MediaRangeType::MediaType("text", "rtf"), vec![]),
                Some(AcceptParams::new(0.8, vec![])),
            ),
        ]);

        let text_plain = MediaType::new("text", "plain", vec![]);
        let text_rtf = MediaType::new("text", "rtf", vec![]);
        let text_css = MediaType::new("text", "css", vec![]);

        assert_eq!(
            a.best_match(vec![&text_plain, &text_rtf].into_iter()),
            Some(&text_rtf)
        );
        assert_eq!(
            a.best_match(vec![&text_rtf, &text_css].into_iter()),
            Some(&text_css)
        );
    }
}
