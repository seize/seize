use crate::http::headers::quality::weight;
use crate::http::{list1, token};
use core::convert::TryFrom;
use std::cmp::Ordering;

use nom::{
    branch::alt,
    bytes::complete::tag,
    combinator::{map, opt},
    sequence::pair,
    IResult,
};

fn accept_charset_item(input: &[u8]) -> IResult<&[u8], AcceptCharsetItem> {
    alt((
        map(tag(b"*"), |_| AcceptCharsetItem::AllCharsets),
        map(token, AcceptCharsetItem::Charset),
    ))(input)
}

fn accept_charset(input: &[u8]) -> IResult<&[u8], AcceptCharset> {
    map(
        list1(&pair(accept_charset_item, opt(weight))),
        AcceptCharset::new,
    )(input)
}

#[derive(Debug, PartialEq, PartialOrd, Ord, Eq)]
pub enum AcceptCharsetItem<'a> {
    Charset(&'a str),
    AllCharsets,
}

impl<'a> AcceptCharsetItem<'a> {
    pub fn is_match<'b>(&self, charset: &'b str) -> bool {
        match self {
            AcceptCharsetItem::AllCharsets => true,
            AcceptCharsetItem::Charset(other) if other.eq_ignore_ascii_case(charset) => true,
            _ => false,
        }
    }
}

#[derive(Debug, PartialEq, PartialOrd)]
pub struct AcceptCharset<'a> {
    charsets: Vec<(AcceptCharsetItem<'a>, Option<f32>)>,
}

impl<'a> AcceptCharset<'a> {
    pub fn new(mut charsets: Vec<(AcceptCharsetItem<'a>, Option<f32>)>) -> Self {
        charsets.sort_by(|(a_r, a_w), (b_r, b_w)| {
            a_r.cmp(&b_r)
                .then(a_w.partial_cmp(&b_w).unwrap_or(Ordering::Equal))
        });

        AcceptCharset { charsets }
    }

    pub fn filtered<'b, I: 'b>(&self, langs: I) -> Vec<&'b str>
    where
        I: Iterator<Item = &'b str>,
    {
        let mut filtered = langs
            .filter_map(|lt| {
                self.charsets
                    .iter()
                    .filter(|(lr, _)| lr.is_match(&lt))
                    .collect::<Vec<_>>()
                    .first()
                    .map(|(_, q)| (lt, q.unwrap_or(1.0)))
            })
            .collect::<Vec<_>>();

        filtered.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap_or(Ordering::Equal).reverse());

        filtered.into_iter().map(|(lt, _)| lt).collect()
    }
}

impl<'a> TryFrom<&'a [u8]> for AcceptCharset<'a> {
    type Error = String;

    fn try_from(s: &'a [u8]) -> Result<Self, String> {
        // TODO: Better Result handling!
        nom::combinator::all_consuming(accept_charset)(s)
            .map(|(_, parsed)| parsed)
            .map_err(|e| format!("{:?}", e))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_accept_charset_try_from_with_multiples() {
        let al = AcceptCharset::new(vec![
            (AcceptCharsetItem::Charset("utf-8"), None),
            (AcceptCharsetItem::Charset("iso-8559-1"), Some(0.5)),
            (AcceptCharsetItem::AllCharsets, Some(0.001)),
        ]);

        assert_eq!(
            AcceptCharset::try_from(b"utf-8, iso-8559-1;q=0.5, *;q=0.001" as &[u8]),
            Ok(al)
        );
    }

    #[test]
    fn accept_charset_filters_and_sorts_langs() {
        let al = AcceptCharset::try_from(b"utf-8, iso-8559-1;q=0.5, *;q=0.01" as &[u8]).unwrap();

        assert_eq!(
            al.filtered(vec!["iso-8559-1", "hjp", "utf-8"].into_iter()),
            vec!["utf-8", "iso-8559-1", "hjp"]
        );
    }
}
