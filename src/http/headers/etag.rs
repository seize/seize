use core::convert::TryFrom;
use std::fmt::Display;
use std::str;

use nom::{
    bytes::complete::{tag, take_while},
    character::is_alphanumeric,
    combinator::{map, opt},
    sequence::{delimited, pair},
    IResult,
};

fn opaque_tag(input: &[u8]) -> IResult<&[u8], &str> {
    map(
        delimited(tag(b"\""), take_while(is_alphanumeric), tag(b"\"")),
        |s| str::from_utf8(s).expect("is_alphanumeric checks should validate UTF8"),
    )(input)
}

fn etag(input: &[u8]) -> IResult<&[u8], ETag> {
    map(
        pair(opt(tag("W/")), opaque_tag),
        |(weak, entity_tag)| match weak {
            Some(_) => ETag::Weak(entity_tag),
            None => ETag::Strong(entity_tag),
        },
    )(input)
}

#[derive(Debug, PartialEq, Eq)]
pub enum ETag<'a> {
    Strong(&'a str),
    Weak(&'a str),
}

impl<'a> TryFrom<&'a [u8]> for ETag<'a> {
    type Error = String;

    fn try_from(value: &'a [u8]) -> Result<ETag<'a>, String> {
        // TODO: Better Result handling!
        nom::combinator::all_consuming(etag)(value)
            .map(|(_, parsed)| parsed)
            .map_err(|e| format!("{:?}", e))
    }
}

impl<'a> Display for ETag<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            Self::Weak(ws) => write!(f, "W/\"{}\"", ws),
            Self::Strong(ss) => write!(f, "\"{}\"", ss),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn weak_etag_try_from_str() {
        assert_eq!(
            ETag::try_from(b"W/\"123b\"" as &[u8]),
            Ok(ETag::Weak("123b"))
        );
    }

    #[test]
    fn strong_etag_try_from_str() {
        assert_eq!(
            ETag::try_from(b"\"123b\"" as &[u8]),
            Ok(ETag::Strong("123b"))
        );
    }

    #[test]
    fn format_weak_tag_is_prefixed_properly() {
        assert_eq!(format!("{}", ETag::Weak("ABC")), "W/\"ABC\"")
    }

    #[test]
    fn format_strong_tag_is_not_prefixed() {
        assert_eq!(format!("{}", ETag::Strong("ABC")), "\"ABC\"")
    }
}
