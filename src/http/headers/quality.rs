use nom::character::is_digit;
use nom::{
    branch::alt,
    bytes::complete::{tag, take_while_m_n},
    character::complete::space0,
    combinator::{map, map_res, opt, recognize},
    sequence::{preceded, tuple},
    IResult,
};
use std::str;

fn qvalue(input: &[u8]) -> IResult<&[u8], f32> {
    map_res(
        recognize(alt((
            tuple((
                tag(b"0"),
                opt(preceded(tag(b"."), take_while_m_n(0, 3, is_digit))),
            )),
            tuple((
                tag(b"1"),
                opt(preceded(tag(b"."), take_while_m_n(0, 3, |c: u8| c == b'0'))),
            )),
        ))),
        |s: &[u8]| {
            str::from_utf8(s)
                .expect("Existing parser should validate utf8 valid")
                .parse::<f32>()
        },
    )(input)
}

pub fn weight(input: &[u8]) -> IResult<&[u8], f32> {
    map(
        tuple((space0, tag(b";"), space0, tag(b"q="), qvalue)),
        |t| t.4,
    )(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_max_qvalue() {
        assert_eq!(qvalue(b"1.000"), Ok((&[] as &[u8], 1.0)));
    }

    #[test]
    fn test_zere_qvalue() {
        assert_eq!(qvalue(b"0"), Ok((&[] as &[u8], 0.0)));
    }

    #[test]
    fn test_one_qvalue() {
        assert_eq!(qvalue(b"1"), Ok((&[] as &[u8], 1.0)));
    }

    #[test]
    fn test_min_qvalue() {
        assert_eq!(qvalue(b"0.001"), Ok((&[] as &[u8], 0.001)));
    }

    #[test]
    fn test_too_long_qvalue() {
        assert_eq!(qvalue(b"0.0001"), Ok((b"1" as &[u8], 0.0)));
    }

    #[test]
    fn test_too_high_decimal_qvalue() {
        assert_eq!(
            qvalue(b"2.0001"),
            Err(nom::Err::Error((
                b"2.0001" as &[u8],
                nom::error::ErrorKind::Tag
            )))
        );
    }

    #[test]
    fn test_too_high_integer_qvalue() {
        assert_eq!(
            qvalue(b"7"),
            Err(nom::Err::Error((b"7" as &[u8], nom::error::ErrorKind::Tag)))
        );
    }

    #[test]
    fn test_basic_weight() {
        assert_eq!(weight(b"   ;     q=0.002"), Ok((&[] as &[u8], 0.002)));
    }
}
