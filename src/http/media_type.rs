use core::convert::TryFrom;
use nom::sequence::pair;

use nom::{bytes::complete::tag, combinator::map, sequence::separated_pair, IResult};

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct Parameter<'a> {
    name: &'a str,
    value: &'a str,
}

impl<'a> Parameter<'a> {
    pub fn new(name: &'a str, value: &'a str) -> Self {
        Parameter { name, value }
    }

    pub fn name(&self) -> &'a str {
        &self.name
    }

    pub fn value(&self) -> &'a str {
        &self.value
    }
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub struct MediaType<'a> {
    r#type: &'a str,
    subtype: &'a str,

    parameters: Vec<Parameter<'a>>,
}

impl<'a> MediaType<'a> {
    pub fn new(r#type: &'a str, subtype: &'a str, parameters: Vec<Parameter<'a>>) -> Self {
        MediaType {
            r#type,
            subtype,
            parameters,
        }
    }

    pub fn type_(&self) -> &'_ str {
        &self.r#type
    }

    pub fn subtype(&self) -> &'_ str {
        &self.subtype
    }

    pub fn parameters(&self) -> &Vec<Parameter<'_>> {
        &self.parameters
    }
}

impl<'a> core::fmt::Display for MediaType<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}/{}", self.type_(), self.subtype())?;
        for (_, p) in self.parameters.iter().enumerate() {
            write!(f, "; {}={}", p.name(), p.value())?;
        }

        Ok(())
    }
}

fn media_type(input: &[u8]) -> IResult<&[u8], MediaType> {
    map(
        pair(
            separated_pair(super::token, tag(b"/"), super::token),
            super::parameter_list,
        ),
        |((t, s), parameters)| MediaType::new(t, s, parameters),
    )(input)
}

impl<'a> TryFrom<&'a [u8]> for MediaType<'a> {
    type Error = String;

    fn try_from(value: &'a [u8]) -> Result<MediaType<'a>, String> {
        // TODO: Better Result handling!
        nom::combinator::all_consuming(media_type)(value)
            .map(|(_, parsed)| parsed)
            .map_err(|e| format!("{:?}", e))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_media_type_try_from_without_parameters() {
        let mt = MediaType::new("text", "plain", vec![]);

        assert_eq!(MediaType::try_from(b"text/plain" as &[u8]), Ok(mt));
    }

    #[test]
    fn test_media_type_try_from_with_parameter() {
        let mt = MediaType::new("text", "plain", vec![Parameter::new("encoding", "utf8")]);

        assert_eq!(
            MediaType::try_from(b"text/plain; encoding=utf8" as &[u8]),
            Ok(mt)
        );
    }

    #[test]
    fn test_media_type_try_from_with_multiple_parameters() {
        let mt = MediaType::new(
            "text",
            "plain",
            vec![
                Parameter::new("encoding", "utf8"),
                Parameter::new("foo", "bar"),
            ],
        );

        assert_eq!(
            MediaType::try_from(b"text/plain; encoding=utf8 ; foo=bar" as &[u8]),
            Ok(mt)
        );
    }

    #[test]
    fn test_media_type_format() {
        assert_eq!(
            format!(
                "{}",
                MediaType::new("text", "plain", vec![Parameter::new("encoding", "utf8")])
            ),
            "text/plain; encoding=utf8"
        );
    }
}
