pub mod headers;
pub mod media_type;

use nom::sequence::separated_pair;
use nom::sequence::tuple;
use std::str;

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while1},
    character::complete::space0,
    combinator::{map, opt},
    error::ParseError,
    multi::many0,
    sequence::{pair, preceded},
    AsChar, Compare, IResult, InputIter, InputLength, InputTake, InputTakeAtPosition,
};

fn is_tchar(chr: u8) -> bool {
    nom::character::is_alphanumeric(chr) || b"!#$%&'*+-.^_`|~".contains(&chr)
}

fn token(input: &[u8]) -> IResult<&[u8], &str> {
    map(
        take_while1(is_tchar),
        |slice| str::from_utf8(slice).expect("Must be UTF-8"), // TODO: Better error surfacing?
    )(input)
}

fn list0<'a, I: 'a, O: 'a, E: ParseError<I> + 'a, F: 'a>(
    f: &'a F,
) -> impl Fn(I) -> IResult<I, Vec<O>, E> + 'a
where
    I: Clone
        + PartialEq
        + InputTakeAtPosition
        + InputLength
        + InputIter
        + InputTake
        + Compare<&'static str>,
    <I as InputTakeAtPosition>::Item: AsChar + Clone,
    F: Fn(I) -> IResult<I, O, E>,
{
    map(
        opt(pair(
            alt((map(tag(","), |_| None), map(f, Some))),
            many0(preceded(pair(space0, tag(",")), opt(preceded(space0, f)))),
        )),
        |o| match o {
            Some((first, rest)) => vec![first]
                .into_iter()
                .chain(rest.into_iter())
                .filter_map(|o| o)
                .collect(),
            None => vec![],
        },
    )
}

fn list1<'a, I: 'a, O: 'a, E: ParseError<I> + 'a, F: 'a>(
    f: &'a F,
) -> impl Fn(I) -> IResult<I, Vec<O>, E> + 'a
where
    I: Clone
        + PartialEq
        + InputTakeAtPosition
        + InputLength
        + InputIter
        + InputTake
        + Compare<&'static str>,
    <I as InputTakeAtPosition>::Item: AsChar + Clone,
    F: Fn(I) -> IResult<I, O, E>,
{
    map(
        preceded(
            many0(pair(tag(","), space0)),
            pair(
                f,
                many0(preceded(pair(space0, tag(",")), opt(preceded(space0, f)))),
            ),
        ),
        |(first, rest)| {
            vec![first]
                .into_iter()
                .chain(rest.into_iter().filter_map(|o| o))
                .collect()
        },
    )
}

fn parameter(input: &[u8]) -> IResult<&[u8], media_type::Parameter> {
    // TODO: quoted-strings!
    map(separated_pair(token, tag(b"="), token), |(n, v)| {
        media_type::Parameter::new(n, v)
    })(input)
}

fn parameter_list(input: &[u8]) -> IResult<&[u8], Vec<media_type::Parameter>> {
    many0(preceded(tuple((space0, tag(b";"), space0)), parameter))(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_numeric_token() {
        assert_eq!(token(b"0"), Ok((&[] as &[u8], "0")));
    }

    #[test]
    fn test_alpha_token() {
        assert_eq!(token(b"abc"), Ok((&[] as &[u8], "abc")));
    }

    #[test]
    fn test_alphanumeric_token() {
        assert_eq!(token(b"abc123"), Ok((&[] as &[u8], "abc123")));
    }

    #[test]
    fn test_special_values_token() {
        assert_eq!(token(b"a+c!"), Ok((&[] as &[u8], "a+c!")));
    }
}
