use super::route::{Builder as RouteBuilder, Route, RouteMatch};

pub struct Router<'a, T> {
    routes: Vec<(Route<'a>, T)>,
}

impl<'a, T> Router<'a, T> {
    pub fn new() -> Self {
        Router { routes: vec![] }
    }

    pub fn from_vec(routes: Vec<(Route<'a>, T)>) -> Self {
        Router { routes }
    }

    pub fn route(self) -> Builder<'a, T> {
        Builder {
            routes: self.routes,
            item: RouteBuilder::new(),
        }
    }

    pub fn find_route<'s, 'p: 's>(&'s self, path: &'p str) -> Option<(RouteMatch<'s>, &T)> {
        self.routes
            .iter()
            .find_map(|(r, i)| r.path_match(path).map(|rm| (rm, i)))
    }
}

pub struct Builder<'a, T> {
    routes: Vec<(Route<'a>, T)>,
    item: RouteBuilder<'a>,
}

impl<'a, T> Builder<'a, T> {
    pub fn segment(self, path: &'a str) -> Self {
        Builder {
            routes: self.routes,
            item: self.item.segment(path),
        }
    }

    pub fn capture_segment(self, path: &'a str) -> Self {
        Builder {
            routes: self.routes,
            item: self.item.capture_segment(path),
        }
    }

    pub fn capture_segment_list(self, path: &'a str) -> Self {
        Builder {
            routes: self.routes,
            item: self.item.capture_segment_list(path),
        }
    }

    pub fn to(self, item: T) -> Router<'a, T> {
        let mut routes = self.routes;
        routes.push((self.item.build(), item));
        Router { routes }
    }
}

#[cfg(test)]
mod tests {
    use super::super::route::{Builder, RouteMatchItem};
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn router_find_match_for_valid_path() {
        let route = Builder::new().segment("home").build();
        let router = Router::from_vec(vec![(route, "HOME")]);

        assert_eq!(
            router.find_route("/home"),
            Some((RouteMatch::empty(), &"HOME"))
        );
    }

    #[test]
    fn router_find_match_for_path_with_captured_segments() {
        let home_route = Builder::new().segment("home").build();
        let name_route = Builder::new()
            .segment("home")
            .capture_segment("name")
            .build();
        let router = Router::from_vec(vec![(home_route, "HOME"), (name_route, "NAME")]);

        let mut expected_route_match = HashMap::new();
        expected_route_match.insert("name", RouteMatchItem::Value("bob"));

        assert_eq!(
            router.find_route("/home/bob"),
            Some((RouteMatch::new(expected_route_match), &"NAME"))
        );
    }

    #[test]
    fn router_fails_to_match_for_other_path() {
        let route = Builder::new().segment("home").build();
        let router = Router::from_vec(vec![(route, "HOME")]);

        assert_eq!(router.find_route("/ho"), None);
    }
}
