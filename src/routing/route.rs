use std::collections::HashMap;

pub struct Route<'a> {
    components: Vec<RouteComponent<'a>>,
}

impl<'a> Route<'a> {
    pub fn path_match<'p: 'a>(&self, path: &'p str) -> Option<RouteMatch<'a>> {
        let result =
            self.components
                .iter()
                .try_fold((HashMap::new(), path), |(mut matches, p), c| {
                    c.match_path(p).map(|(i, r)| {
                        if let Some((k, v)) = i {
                            matches.insert(k, v);
                        };
                        (matches, r)
                    })
                });

        match result {
            Some((items, "")) => Some(RouteMatch::new(items)),
            _ => None,
        }
    }
}

impl<'a> std::fmt::Display for Route<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        for c in self.components.iter() {
            write!(f, "{}", c)?;
        }
        Ok(())
    }
}

pub struct Builder<'a> {
    components: Vec<RouteComponent<'a>>,
}

impl<'a> Builder<'a> {
    pub fn new() -> Self {
        Builder { components: vec![] }
    }

    pub fn segment(mut self, path: &'a str) -> Self {
        self.components.push(RouteComponent::PathSegment(path));

        self
    }

    pub fn capture_segment(mut self, path: &'a str) -> Self {
        self.components
            .push(RouteComponent::PathSegmentCapture(path, false));

        self
    }

    pub fn capture_segment_list(mut self, path: &'a str) -> Self {
        self.components
            .push(RouteComponent::PathSegmentCapture(path, true));

        self
    }

    pub fn build(self) -> Route<'a> {
        Route {
            components: self.components,
        }
    }
}

enum RouteComponent<'a> {
    PathSegment(&'a str),
    PathSegmentCapture(&'a str, bool),
}

impl<'a> RouteComponent<'a> {
    fn match_path<'p>(
        &self,
        path: &'p str,
    ) -> Option<(Option<(&'a str, RouteMatchItem<'p>)>, &'p str)> {
        match self {
            Self::PathSegment(ps) => {
                let match_item = ["/", ps].concat();
                if path.starts_with(&match_item) {
                    path.get((ps.len() + 1)..).map(|p| (None, p))
                } else {
                    None
                }
            }
            Self::PathSegmentCapture(name, false) => {
                if path.starts_with("/") {
                    path.get(1..).map(|s| {
                        let split_idx = s.find("/").unwrap_or(s.len());
                        let (capture, rest) = s.split_at(split_idx);
                        (Some((*name, RouteMatchItem::Value(capture))), rest)
                    })
                } else {
                    None
                }
            }
            Self::PathSegmentCapture(name, true) => {
                if path.starts_with("/") {
                    path.get(1..)
                        .map(|s| s.split("/"))
                        .map(|vals| (Some((*name, RouteMatchItem::List(vals.collect()))), ""))
                } else {
                    None
                }
            }
        }
    }
}

impl<'a> std::fmt::Display for RouteComponent<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self {
            Self::PathSegment(path) => write!(f, "/{}", path)?,
            Self::PathSegmentCapture(name, false) => write!(f, "{{/{}}}", name)?,
            Self::PathSegmentCapture(name, true) => write!(f, "{{/{}*}}", name)?,
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq)]
pub struct RouteMatch<'a> {
    items: HashMap<&'a str, RouteMatchItem<'a>>,
}

impl<'a> RouteMatch<'a> {
    pub fn new(items: HashMap<&'a str, RouteMatchItem<'a>>) -> RouteMatch<'a> {
        RouteMatch { items }
    }

    pub fn empty() -> RouteMatch<'a> {
        RouteMatch {
            items: HashMap::new(),
        }
    }

    pub fn get(&self, name: &str) -> Option<&RouteMatchItem> {
        self.items.get(name)
    }
}

#[derive(Debug, PartialEq)]
pub enum RouteMatchItem<'a> {
    Value(&'a str),
    List(Vec<&'a str>),
}

#[cfg(test)]
mod tests {
    use super::*;

    fn empty_match<'a>() -> RouteMatch<'a> {
        RouteMatch {
            items: HashMap::new(),
        }
    }

    #[test]
    fn match_static() {
        let route = Builder::new().segment("home").build();
        assert_eq!(route.path_match("/home"), Some(empty_match()));
    }

    #[test]
    fn match_static_deep() {
        let route = Builder::new().segment("home").segment("uncle").build();
        assert_eq!(route.path_match("/home/uncle"), Some(empty_match()));
    }

    #[test]
    fn match_route_with_segment_capture() {
        let route = Builder::new()
            .segment("home")
            .capture_segment("name")
            .build();
        let mut map = HashMap::new();
        map.insert("name", RouteMatchItem::Value("bob"));
        let m = RouteMatch::new(map);
        assert_eq!(route.path_match("/home/bob"), Some(m));
    }

    #[test]
    fn match_route_with_multiple_segment_capture() {
        let route = Builder::new()
            .segment("home")
            .capture_segment_list("name")
            .build();

        let mut map = HashMap::new();
        map.insert("name", RouteMatchItem::List(vec!["bob", "uncle"]));
        let m = RouteMatch::new(map);
        assert_eq!(route.path_match("/home/bob/uncle"), Some(m));
    }

    #[test]
    fn route_format_creates_uri_template_str() {
        let route = Builder::new()
            .segment("home")
            .capture_segment("name")
            .capture_segment_list("names")
            .build();

        assert_eq!(format!("{}", route), "/home{/name}{/names*}");
    }
}
