use futures::future::FutureExt;
use hyper::header::HeaderValue;
use std::future::Future;

use crate::http::headers::{Accept, AcceptCharset, AcceptLanguage};
use crate::resource::{Resource, ResourceContext, ResourceError, ResourceRequest};
use hyper::{Body, Response, StatusCode};
use std::convert::TryFrom;

pub fn start<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> impl Future<Output = Result<Response<Body>, ResourceError>> + 'a {
    v3c3(resource, context, request, body, response).boxed()
}

flow_has_header!(v3c3, hyper::header::ACCEPT, v3c4, v3c3a);

// Default to first provided content type, if client doesn't send `Accept` header.
pub async fn v3c3a<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3c3a");
    let (ct_provided, res) = resource.content_types_provided(request, response).await?;

    context.selected_media_type = ct_provided.keys().next().cloned();
    drop(ct_provided);
    match context.selected_media_type {
        Some(_) => v3d4(resource, context, request, body, res).await,
        None => v3c7(resource, context, request, res).await,
    }
}

pub async fn v3c4<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3c4");
    let (ct_provided, res) = resource.content_types_provided(request, response).await?;

    let types = Accept::try_from(header.as_bytes())
        .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))?;

    context.selected_media_type = types.best_match(ct_provided.keys()).cloned();
    drop(ct_provided);

    match context.selected_media_type {
        Some(_) => v3d4(resource, context, request, body, res).await,
        None => v3c7(resource, context, request, res).await,
    }
}

flow_terminus!(v3c7, 406);

flow_has_header!(v3d4, hyper::header::ACCEPT_LANGUAGE, v3d5, v3e5);

pub async fn v3d5<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3d5");
    let (langs_provided, mut res) = resource.languages_provided(request, response).await?;

    let al = AcceptLanguage::try_from(header.as_bytes())
        .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))?;

    context.selected_language = al.filtered(langs_provided.iter()).first().cloned().cloned();

    match (langs_provided.len(), context.selected_language.as_ref()) {
        (0, _) => v3e5(resource, context, request, body, res).await,
        (_, Some(l)) => {
            // TODO: This this here? Or do we need to get past other checks first?!
            res = res.header(hyper::header::CONTENT_LANGUAGE, format!("{}", l));
            v3e5(resource, context, request, body, res).await
        }
        (_, None) => {
            if cfg!(feature = "strict-accept-language") {
                v3c7(resource, context, request, res).await
            } else {
                v3e5(resource, context, request, body, res).await
            }
        }
    }
}

flow_has_header!(v3e5, hyper::header::ACCEPT_CHARSET, v3e6, v3f6);

pub async fn v3e6<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3e6");
    let (charsets_provided, res) = resource.charsets_provided(request, response).await?;

    let ac = AcceptCharset::try_from(header.as_bytes())
        .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))?;

    let charsets_len = charsets_provided.len();
    context.selected_charset = ac.filtered(charsets_provided.into_iter()).first().cloned();

    match (charsets_len, context.selected_charset.as_ref()) {
        (0, _) => v3f6(resource, context, request, body, res).await,
        (_, Some(_cs)) => v3f6(resource, context, request, body, res).await,
        (_, None) => {
            if cfg!(feature = "strict-accept-charset") {
                v3c7(resource, context, request, res).await
            } else {
                v3f6(resource, context, request, body, res).await
            }
        }
    }
}

flow_has_header!(v3f6, hyper::header::ACCEPT_ENCODING, v3f7, v3g7);

pub async fn v3f7<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    _header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3f7");
    v3g7(resource, context, request, body, response).await
}

flow_bool_check_node!(
    v3g7,
    resource_exists,
    super::existent_resource::start,
    super::nonexistent_resource::start
);
