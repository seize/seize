extern crate httpdate;

use crate::resource::{Resource, ResourceContext, ResourceError, ResourceRequest};
use hyper::{Body, Method, Response};

pub async fn v3p11<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3p11");
    if response
        .headers_ref()
        .map(|hs| hs.contains_key(hyper::header::LOCATION))
        .unwrap_or(false)
    {
        v3q12(resource, context, request, response).await
    } else {
        v3o20(resource, context, request, response).await
    }
}

flow_terminus!(v3q12, 201);

pub async fn v3o20<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3o20");
    if context.body.is_some() {
        v3o18(resource, context, request, response).await
    } else {
        v3o21(resource, context, request, response).await
    }
}

flow_terminus!(v3o21, 204);

pub async fn v3o18<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3o18");

    let mut res = response;
    let method = request.method().clone();
    match method {
        Method::GET | Method::HEAD => {
            let ct = context
                .selected_media_type
                .take()
                .expect("Should have returned 406 if no matching type");
            let (mut body_generators, res2) = resource.content_types_provided(request, res).await?;
            res = res2;
            let new_body = body_generators
                .remove(&ct)
                .expect("Should only have selected provided types")
                .await?;

            drop(body_generators);

            context.selected_media_type = Some(ct);
            context.body = Some(new_body);
        }
        _ => {}
    }

    let (mult, res) = resource.multiple_representations(request, res).await?;

    if mult {
        v3op19(resource, context, request, res).await
    } else {
        v3n18(resource, context, request, res).await
    }
}

flow_terminus!(v3op19, 300);
flow_terminus!(v3n18, 200);
