extern crate httpdate;

use futures::future::FutureExt;
use std::convert::TryFrom;
use std::future::Future;
use std::time::SystemTime;

use httpdate::parse_http_date;

use crate::http::headers::ETag;
use crate::resource::{DeleteStatus, Resource, ResourceContext, ResourceError, ResourceRequest};
use hyper::header::HeaderValue;
use hyper::{Body, Method, Response, StatusCode};

pub fn start<'a, 'b: 'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> impl Future<Output = Result<Response<Body>, ResourceError>> + 'a {
    v3g8(resource, context, request, body, response).boxed()
}

flow_has_header!(v3g8, hyper::header::IF_MATCH, v3g9, v3h10);

async fn v3g9<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3g9");
    if header == HeaderValue::from_static("*") {
        v3h10(resource, context, request, body, response).await
    } else {
        v3g11(resource, context, request, body, response, header).await
    }
}

flow_has_header!(v3h10, hyper::header::IF_UNMODIFIED_SINCE, v3h11, v3i12);

async fn v3h11<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3h11");
    let ius = parse_http_date(
        header
            .to_str()
            .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))?,
    );

    match ius {
        Ok(d) => v3h12(resource, context, request, body, response, d).await,
        _ => v3i12(resource, context, request, body, response).await,
    }
}

async fn v3h12<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    ius: SystemTime,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3h12");
    let (lm_opt, res) = resource.last_modified(request, response).await?;

    if lm_opt.map(|lm| lm > ius).unwrap_or(false) {
        v3h18(resource, context, request, res).await
    } else {
        v3i12(resource, context, request, body, res).await
    }
}

async fn v3g11<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3g11");
    let (res_etag, res) = resource.etag(request, response).await?;

    let req_etag = ETag::try_from(header.as_bytes())
        .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))?;

    if Some(req_etag) == res_etag {
        v3h10(resource, context, request, body, res).await
    } else {
        v3h18(resource, context, request, res).await
    }
}

flow_has_header!(v3i12, hyper::header::IF_NONE_MATCH, v3i13, v3l13);

async fn v3i13<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3i13");
    if header == HeaderValue::from_static("*") {
        v3j18(resource, context, request, body, response).await
    } else {
        v3k13(resource, context, request, body, response, header).await
    }
}

async fn v3k13<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3k13");
    let (res_etag, res) = resource.etag(request, response).await?;

    let req_etag = ETag::try_from(header.as_bytes())
        .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))?;

    if Some(req_etag) == res_etag {
        v3j18(resource, context, request, body, res).await
    } else {
        v3l13(resource, context, request, body, res).await
    }
}

async fn v3j18<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    _body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3j18");
    let method = request.method();

    match *method {
        Method::GET | Method::HEAD => v3l18(resource, context, request, response).await,
        _ => v3h18(resource, context, request, response).await,
    }
}

flow_has_header!(v3l13, hyper::header::IF_MODIFIED_SINCE, v3l14, v3m16);

async fn v3l14<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    header: HeaderValue,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3l14");
    let ims = parse_http_date(
        header
            .to_str()
            .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))?,
    );

    match ims {
        Ok(d) => v3l15(resource, context, request, body, response, d).await,
        _ => v3m16(resource, context, request, body, response).await,
    }
}

flow_terminus!(v3l18, 304);

async fn v3l15<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    ims: SystemTime,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3l15");
    if ims > SystemTime::now() {
        v3m16(resource, context, request, body, response).await
    } else {
        v3l17(resource, context, request, body, response, ims).await
    }
}

async fn v3l17<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    ims: SystemTime,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3l17");
    let (lm_opt, res) = resource.last_modified(request, response).await?;
    if lm_opt.map(|lm| lm > ims).unwrap_or(true) {
        v3m16(resource, context, request, body, res).await
    } else {
        v3l18(resource, context, request, res).await
    }
}

flow_terminus!(v3h18, 412);

flow_is_request_method!(v3m16, DELETE, v3m20, v3n16);

async fn v3m20<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    _body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3m20");
    let (result, res) = resource.do_delete(request, response).await?;
    match result {
        DeleteStatus::Accepted => v3m21(resource, context, request, res).await,
        DeleteStatus::Enacted => super::completion::v3o20(resource, context, request, res).await,
    }
}

flow_terminus!(v3m21, 202);

flow_is_request_method!(v3n16, POST, super::post_processing::v3n11, v3o16);

async fn v3o16<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3o16");
    if request.method() == Method::PUT {
        v3o14(resource, context, request, body, response).await
    } else {
        super::completion::v3o18(resource, context, request, response).await
    }
}

flow_bool_check_node!(v3o14, is_conflict, v3o14conflict, v3o14accept);

flow_terminus!(v3o14conflict, 409, true);

async fn v3o14accept<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3o14accept");

    let res =
        super::accept::accept_helper(resource, context, request, body, response, request.uri())
            .await?;

    super::completion::v3p11(resource, context, request, res).await
}
