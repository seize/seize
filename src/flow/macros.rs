#[macro_export]
macro_rules! flow_terminus {
    ($n:ident, $s:expr) => {
        async fn $n<'a>(
            _: &'a mut (dyn Resource),
            context: &'a mut ResourceContext<'a>,
            _request: &'a ResourceRequest<'a>,
            response: http::response::Builder,
        ) -> Result<Response<Body>, ResourceError> {
            context.log_node(stringify!($n));
            let mut res = context.add_flow_log_header(response).status($s);
            if let Some(ct) = context.selected_media_type.take() {
                res = res.header("content-type", format!("{}", ct));
            }
            Ok(res.body(context.body.take().unwrap_or(Body::empty()))?)
        }
    };
    ($n:ident, $s:expr, true) => {
        async fn $n<'a>(
            _: &'a mut (dyn Resource),
            context: &'a mut ResourceContext<'a>,
            _request: &'a ResourceRequest<'a>,
            _body: hyper::Body,
            response: http::response::Builder,
        ) -> Result<Response<Body>, ResourceError> {
            context.log_node(stringify!($n));
            let mut res = context.add_flow_log_header(response).status($s);
            if let Some(ct) = context.selected_media_type.take() {
                res = res.header("content-type", format!("{}", ct));
            }
            Ok(res.body(context.body.take().unwrap_or(Body::empty()))?)
        }
    };
}

#[macro_export]
macro_rules! flow_bool_check {
    ($res:ident, $ctx:ident, $req:ident, $body:ident, $resp:ident, $check:expr, $pass:path, $fail:path) => {
        if $check {
            $pass($res, $ctx, $req, $body, $resp).await
        } else {
            $fail($res, $ctx, $req, $body, $resp).await
        }
    };
}

#[macro_export]
macro_rules! flow_is_request_method {
    ($n:ident, $v:ident, $pass:path, $fail:path) => {
        async fn $n<'a>(
            resource: &'a mut (dyn Resource),
            context: &'a mut ResourceContext<'a>,
            request: &'a ResourceRequest<'a>,
            body: hyper::Body,
            response: http::response::Builder,
        ) -> Result<Response<Body>, ResourceError> {
            context.log_node(stringify!($n));

            flow_bool_check!(
                resource,
                context,
                request,
                body,
                response,
                request.method() == Method::$v,
                $pass,
                $fail
            )
        }
    };
}

#[macro_export]
macro_rules! flow_has_header {
    ($n:ident, $h:expr, $pass:path, $fail:path) => {
        async fn $n<'a>(
            resource: &'a mut (dyn Resource),
            context: &'a mut ResourceContext<'a>,
            request: &'a ResourceRequest<'a>,
            body: hyper::Body,
            response: http::response::Builder,
        ) -> Result<Response<Body>, ResourceError> {
            context.log_node(stringify!($n));
            let h = request.headers().get($h).cloned();
            match h {
                Some(header) => $pass(resource, context, request, body, response, header).await,
                None => $fail(resource, context, request, body, response).await,
            }
        }
    };
}

#[macro_export]
macro_rules! flow_has_header_value {
    ($n:ident, $v:expr, $pass:path, $fail:path) => {
        async fn $n<'a>(
            resource: &'a mut (dyn Resource),
            context: &'a mut ResourceContext<'a>,
            request: &'a ResourceRequest<'a>,
            body: hyper::Body,
            response: http::response::Builder,
            header: HeaderValue,
        ) -> Result<Response<Body>, ResourceError> {
            context.log_node(stringify!($n));
            if header == HeaderValue::from_static($v) {
                $pass(resource, context, request, body, response).await
            } else {
                $fail(resource, context, request, body, response).await
            }
        }
    };
}

#[macro_export]
macro_rules! flow_bool_check_node {
    ($n:ident, $f:ident, $pass:path, $fail:path) => {
        async fn $n<'a>(
            resource: &'a mut (dyn Resource),
            context: &'a mut ResourceContext<'a>,
            request: &'a ResourceRequest<'a>,
            body: hyper::Body,
            response: http::response::Builder,
        ) -> Result<Response<Body>, ResourceError> {
            context.log_node(stringify!($n));
            let (res, resp) = resource.$f(request, response).await?;
            flow_bool_check!(resource, context, request, body, resp, res, $pass, $fail)
        }
    };
}
