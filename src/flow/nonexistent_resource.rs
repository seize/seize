use futures::future::FutureExt;
use std::future::Future;

use crate::resource::{Resource, ResourceContext, ResourceError, ResourceRequest};
use hyper::header::HeaderValue;
use hyper::{Body, Method, Response};

pub fn start<'a, 'b: 'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> impl Future<Output = Result<Response<Body>, ResourceError>> + 'a {
    v3h7(resource, context, request, body, response).boxed()
}

flow_has_header!(v3h7, hyper::header::IF_MATCH, v3h7b, v3i7);
flow_has_header_value!(v3h7b, "*", v3h6, v3i7);

flow_terminus!(v3h6, 412, true);

flow_is_request_method!(v3i7, PUT, v3i4, v3k7);

flow_bool_check_node!(v3k7, resource_previously_existed, v3k5, v3l7);

async fn v3k5<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    let (moved_uri, res) = resource.moved_permanently(request, response).await?;

    match moved_uri {
        Some(u) => v3k4(resource, context, request, res, u).await,
        None => v3l5(resource, context, request, body, res).await,
    }
}

async fn v3l5<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    let (moved_uri, res) = resource.moved_temporarily(request, response).await?;

    match moved_uri {
        Some(u) => v3l4(resource, context, request, res, u).await,
        None => v3m5(resource, context, request, body, res).await,
    }
}

flow_is_request_method!(v3m5, POST, v3n5, v3n4);
flow_bool_check_node!(v3n5, allow_missing_post, v3n11, v3n4);
flow_terminus!(v3n4, 410, true);

flow_is_request_method!(v3l7, POST, v3m7, v3m8);

flow_bool_check_node!(v3m7, allow_missing_post, v3n11, v3m8);

flow_terminus!(v3m8, 404, true);

flow_terminus!(v3n11, 501, true); // TODO: IMPLEMENT ME!

async fn v3i4<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    let (moved_uri, res) = resource.moved_permanently(request, response).await?;

    match moved_uri {
        Some(u) => v3k4(resource, context, request, res, u).await,
        None => v3p3(resource, context, request, body, res).await,
    }
}

async fn v3k4<'a>(
    _resource: &'a mut (dyn Resource),
    _context: &'a mut ResourceContext<'a>,
    _request: &'a ResourceRequest<'a>,
    response: http::response::Builder,
    uri: hyper::Uri,
) -> Result<Response<Body>, ResourceError> {
    Ok(response
        .status(301)
        .header(hyper::header::LOCATION, format!("{}", uri))
        .body(Body::empty())?)
}

async fn v3l4<'a>(
    _resource: &'a mut (dyn Resource),
    _context: &'a mut ResourceContext<'a>,
    _request: &'a ResourceRequest<'a>,
    response: http::response::Builder,
    uri: hyper::Uri,
) -> Result<Response<Body>, ResourceError> {
    Ok(response
        .status(307)
        .header(hyper::header::LOCATION, format!("{}", uri))
        .body(Body::empty())?)
}

async fn v3p3<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    _body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    let (conflict, res) = resource.is_conflict(request, response).await?;

    if conflict {
        v3p2(resource, context, request, res).await
    } else {
        super::completion::v3p11(resource, context, request, res).await
    }
}

flow_terminus!(v3p2, 409);
