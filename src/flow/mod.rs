use crate::resource::{Resource, ResourceContext, ResourceError, ResourceRequest};
use hyper::{Body, Response};

#[macro_use]
mod macros;

mod accept;
mod accept_checks;
mod completion;
mod existent_resource;
mod nonexistent_resource;
mod post_processing;
mod validation;

pub async fn start<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    validation::start(resource, context, request, body, response).await
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::http::media_type::MediaType;
    use crate::resource::{FutureResourceResponse, ResourceResponse};
    use async_trait::async_trait;
    use futures::future::{ok, FutureExt};
    use hyper::header::HeaderValue;
    use std::collections::HashMap;

    struct BlankResource;

    impl Resource for BlankResource {}

    #[test]
    pub fn flow_returns_not_acceptable_for_simple_resource_without_any_content() {
        let mut res = BlankResource {};

        let mut ctx = ResourceContext::new();
        let (parts, body) = hyper::Request::get("/")
            .body(hyper::Body::empty())
            .unwrap()
            .into_parts();
        let route = crate::routing::route::RouteMatch::empty();
        let res_req = ResourceRequest::new(route, &parts);

        let result = futures::executor::block_on(start(
            &mut res,
            &mut ctx,
            &res_req,
            body,
            http::response::Builder::new(),
        ));

        assert_eq!(result.unwrap().status(), http::StatusCode::NOT_ACCEPTABLE);
    }

    struct StaticResource;

    #[async_trait]
    impl Resource for StaticResource {
        async fn content_types_provided<'s, 'a>(
            &'s mut self,
            _request: &'s ResourceRequest<'s>,
            response: http::response::Builder,
        ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
            let mut map = HashMap::new();

            map.insert(
                MediaType::new("text", "html", vec![]),
                ok(Body::from("<html>Hi</html>")).boxed(),
            );
            Ok((map, response))
        }
    }

    #[test]
    pub fn flow_returns_ok_for_static_resource_with_fixed_content() {
        let mut res = StaticResource {};

        let mut ctx = ResourceContext::new();
        let (parts, body) = hyper::Request::get("/")
            .body(hyper::Body::empty())
            .unwrap()
            .into_parts();
        let route = crate::routing::route::RouteMatch::empty();
        let res_req = ResourceRequest::new(route, &parts);

        futures::executor::block_on(async {
            let result = start(
                &mut res,
                &mut ctx,
                &res_req,
                body,
                http::response::Builder::new(),
            )
            .await
            .unwrap();

            assert_eq!(result.status(), http::StatusCode::OK);
            assert_eq!(
                result.headers().get("content-type"),
                Some(&HeaderValue::from_static("text/html"))
            );

            assert_eq!(
                hyper::body::to_bytes(result.into_body()).await.unwrap(),
                hyper::body::to_bytes(Body::from("<html>Hi</html>"))
                    .await
                    .unwrap()
            );
        });
    }
}
