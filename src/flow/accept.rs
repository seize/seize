use crate::resource::{AcceptStatus, Resource, ResourceContext, ResourceError, ResourceRequest};
use hyper::StatusCode;

pub async fn accept_helper<'a, 'b: 'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'b>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
    uri: &'a http::Uri,
) -> Result<http::response::Builder, ResourceError> {
    let (accept_status, res) = resource
        .content_type_accepter(request, body, response, uri)
        .await?;

    match accept_status {
        AcceptStatus::Unsupported => Err(ResourceError::Halt(StatusCode::UNSUPPORTED_MEDIA_TYPE)),
        AcceptStatus::Accepted(Some((mt, b))) => {
            context.body = Some(b);
            Ok(res.header(hyper::header::CONTENT_TYPE, format!("{}", mt)))
        }
        _ => Ok(res),
    }
}
