use crate::resource::{Resource, ResourceContext, ResourceError, ResourceRequest};
use hyper::{Body, Response};

pub async fn start<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    v3b13(resource, context, request, body, response).await
}

flow_bool_check_node!(v3b13, service_available, v3b12, v3a13);

flow_terminus!(v3a13, 503, true);

async fn v3b12<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3b12");
    let (methods, res) = resource.known_methods(request, response).await?;

    flow_bool_check!(
        resource,
        context,
        request,
        body,
        res,
        methods.contains(request.method()),
        v3b11,
        v3a12
    )
}

flow_terminus!(v3a12, 501, true);

async fn v3b11<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3b11");

    let (too_long, res) = resource
        .request_uri_too_long(request, response, request.uri())
        .await?;
    flow_bool_check!(resource, context, request, body, res, too_long, v3a11, v3b10)
}

flow_terminus!(v3a11, 414, true);

async fn v3b10<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3b10");
    let (methods, res) = resource.allowed_methods(request, response).await?;

    flow_bool_check!(
        resource,
        context,
        request,
        body,
        res,
        methods.contains(request.method()),
        v3b9,
        v3a10
    )
}

flow_terminus!(v3a10, 405, true);

flow_bool_check_node!(v3b9, malformed_request, v3a9, v3b8);

flow_terminus!(v3a9, 400, true);

async fn v3b8<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3b8");
    let auth = request.headers().get(hyper::header::AUTHORIZATION);

    let (authorized, res) = resource.is_authorized(request, response, auth).await?;

    flow_bool_check!(resource, context, request, body, res, authorized, v3b7, v3a8)
}

flow_terminus!(v3a8, 401, true); // TODO: Needs extra headers!

flow_bool_check_node!(v3b7, forbidden, v3a7, v3b6);
flow_terminus!(v3a7, 403, true);

flow_bool_check_node!(v3b6, valid_content_headers, v3b5, v3a6);

flow_terminus!(v3a6, 501, true);

async fn v3b5<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3b5");
    let ct = request.headers().get(hyper::header::CONTENT_TYPE);

    let (known_ct, res) = resource.known_content_type(request, response, ct).await?;
    flow_bool_check!(resource, context, request, body, res, known_ct, v3b4, v3a5)
}

flow_terminus!(v3a5, 415, true);

async fn v3b4<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3b4");
    let cl = request
        .headers()
        .get(hyper::header::CONTENT_LENGTH)
        .and_then(|h| h.to_str().ok())
        .and_then(|h| h.parse::<u64>().ok());

    let (valid_length, res) = resource.valid_entity_length(request, response, cl).await?;

    flow_bool_check!(
        resource,
        context,
        request,
        body,
        res,
        valid_length,
        v3b3,
        v3a4
    )
}

flow_terminus!(v3a4, 413, true);

async fn v3b3<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    context.log_node("v3b3");
    let method = request.method();

    flow_bool_check!(
        resource,
        context,
        request,
        body,
        response,
        method == hyper::http::Method::OPTIONS,
        v3a3,
        super::accept_checks::start
    )
}

async fn v3a3<'a>(
    resource: &'a mut (dyn Resource),
    _context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    _body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    let (option_headers, res) = resource.options(request, response).await?;

    Ok(option_headers
        .into_iter()
        .fold(res, |r, (n, v)| r.header(n, v))
        .status(200)
        .body(Body::empty())?)
}
