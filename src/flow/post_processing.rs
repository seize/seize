use crate::resource::{Resource, ResourceContext, ResourceError, ResourceRequest};
use http::uri::Uri;
use hyper::{Body, Response};

pub async fn v3n11<'a>(
    resource: &'a mut (dyn Resource),
    context: &'a mut ResourceContext<'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
    response: http::response::Builder,
) -> Result<Response<Body>, ResourceError> {
    let (path_opt, mut res) = resource.post_create_path(request, response).await?;
    match path_opt {
        Some(path) => {
            let orig_uri = request.uri();
            let updated_uri: Uri = Uri::builder()
                .scheme(orig_uri.scheme().map(|s| s.as_str()).unwrap_or(""))
                .authority(orig_uri.authority().map(|s| s.as_str()).unwrap_or(""))
                .path_and_query(path.as_str())
                .build()
                .unwrap();
            res = super::accept::accept_helper(resource, context, request, body, res, &updated_uri)
                .await?;
        }
        _ => {
            let (post_res, res2) = resource.process_post(request, body, res).await?;
            res = res2;

            let (ct, body) = post_res
                .map(|(ct, body)| (Some(ct), Some(body)))
                .unwrap_or((None, None));

            context.selected_media_type = ct;
            context.body = body;
        }
    }

    super::completion::v3p11(resource, context, request, res).await
}
