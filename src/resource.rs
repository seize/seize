extern crate async_trait;
extern crate futures;
extern crate http;
extern crate hyper;

use crate::http::headers::ETag;
use crate::http::media_type::MediaType;
use crate::language::LanguageTag;
use async_trait::async_trait;
use futures::future::FutureExt;
use http::response::Builder;
use http::uri::PathAndQuery;
use hyper::http::Method;
use hyper::{Body, Request, Response, StatusCode};
use std::collections::{HashMap, HashSet};
use std::convert::Infallible;
use std::future::Future;
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use std::time::SystemTime;

#[derive(Debug)]
pub enum ResourceError {
    Halt(StatusCode),
    Err(http::Error),
}

impl From<http::Error> for ResourceError {
    fn from(e: http::Error) -> Self {
        ResourceError::Err(e)
    }
}

#[derive(Debug)]
pub struct ResourceContext<'a> {
    pub selected_media_type: Option<MediaType<'a>>,
    pub selected_language: Option<LanguageTag<'a>>,
    pub selected_charset: Option<&'a str>,
    pub body: Option<Body>, // TODO:: Generic over Payload!
    flow_trace: Vec<&'static str>,
}

impl<'a> ResourceContext<'a> {
    pub fn new() -> Self {
        ResourceContext {
            selected_media_type: None,
            selected_language: None,
            selected_charset: None,
            body: None,
            flow_trace: vec![],
        }
    }
    pub fn log_node(&mut self, node: &'static str) {
        self.flow_trace.push(node);
    }

    pub fn add_flow_log_header(&mut self, response: Builder) -> Builder {
        response.header("x-seize-trace", self.flow_trace.join(","))
    }
}

// pub type ResourceRequest = hyper::Request<hyper::Body>;
pub struct ResourceRequest<'a> {
    route: crate::routing::route::RouteMatch<'a>,
    parts: &'a http::request::Parts,
}

impl<'a> ResourceRequest<'a> {
    pub fn new(
        route: crate::routing::route::RouteMatch<'a>,
        parts: &'a http::request::Parts,
    ) -> Self {
        ResourceRequest { route, parts }
    }

    pub fn route(&self) -> &crate::routing::route::RouteMatch<'a> {
        &self.route
    }

    pub fn version(&self) -> &http::Version {
        &self.parts.version
    }

    pub fn uri(&self) -> &http::Uri {
        &self.parts.uri
    }

    pub fn method(&self) -> &http::Method {
        &self.parts.method
    }

    pub fn headers(&self) -> &http::HeaderMap {
        &self.parts.headers
    }
}

pub type ResourceResponse<T> = Result<(T, Builder), ResourceError>;

pub type FutureResourceResponse<'a, T> =
    Pin<Box<dyn Future<Output = Result<T, ResourceError>> + 'a + Send>>;

pub enum DeleteStatus {
    Enacted,
    Accepted,
}

#[derive(Debug)]
pub enum AcceptStatus<'a> {
    Unsupported,
    Accepted(Option<(MediaType<'a>, Body)>),
}

#[async_trait]
pub trait Resource: Send {
    async fn service_available(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((true, resp))
    }

    async fn resource_exists(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((true, resp))
    }

    async fn resource_previously_existed(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((false, resp))
    }

    async fn known_methods(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<HashSet<hyper::http::Method>> {
        Ok((
            [
                Method::GET,
                Method::HEAD,
                Method::POST,
                Method::PUT,
                Method::DELETE,
                Method::TRACE,
                Method::CONNECT,
                Method::OPTIONS,
            ]
            .iter()
            .cloned()
            .collect(),
            resp,
        ))
    }

    async fn request_uri_too_long(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
        _uri: &'_ hyper::Uri,
    ) -> ResourceResponse<bool> {
        Ok((false, resp))
    }

    async fn allowed_methods(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<HashSet<hyper::http::Method>> {
        Ok((
            [Method::OPTIONS, Method::GET, Method::HEAD]
                .iter()
                .cloned()
                .collect(),
            resp,
        ))
    }

    async fn malformed_request(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((false, resp))
    }

    async fn is_authorized(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
        _h: Option<&'_ hyper::header::HeaderValue>,
    ) -> ResourceResponse<bool> {
        Ok((true, resp))
    }

    async fn forbidden(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((false, resp))
    }

    async fn valid_content_headers(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((true, resp))
    }

    async fn known_content_type(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
        _h: Option<&'_ hyper::header::HeaderValue>,
    ) -> ResourceResponse<bool> {
        Ok((true, resp))
    }

    async fn valid_entity_length(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
        _l: Option<u64>,
    ) -> ResourceResponse<bool> {
        Ok((true, resp))
    }

    async fn options(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<Vec<(hyper::header::HeaderName, hyper::header::HeaderValue)>> {
        Ok((vec![], resp))
    }

    async fn etag<'a>(
        &'_ mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<Option<ETag<'a>>> {
        Ok((None, resp))
    }

    async fn last_modified(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<Option<SystemTime>> {
        Ok((None, resp))
    }

    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<
        HashMap<crate::http::media_type::MediaType<'a>, FutureResourceResponse<'s, Body>>,
    > {
        Ok((HashMap::default(), response))
    }

    async fn content_type_accepter<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        _body: hyper::Body,
        response: http::response::Builder,
        _uri: &'s http::Uri,
    ) -> ResourceResponse<AcceptStatus<'a>> {
        Ok((AcceptStatus::Unsupported, response))
    }

    async fn languages_provided<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Vec<LanguageTag<'a>>> {
        Ok((vec![], response))
    }

    async fn charsets_provided<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Vec<&'a str>> {
        Ok((vec![], response))
    }

    async fn do_delete(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        _resp: http::response::Builder,
    ) -> ResourceResponse<DeleteStatus> {
        Err(ResourceError::Halt(StatusCode::NOT_IMPLEMENTED))
    }

    async fn moved_permanently(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<hyper::Uri>> {
        Ok((None, response))
    }

    async fn moved_temporarily(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<hyper::Uri>> {
        Ok((None, response))
    }

    async fn is_conflict(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((false, resp))
    }

    async fn allow_missing_post(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((false, resp))
    }

    async fn multiple_representations(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((false, resp))
    }

    async fn post_create_path<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<PathAndQuery>> {
        Ok((None, response))
    }

    async fn process_post<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        _body: hyper::Body,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<(MediaType<'a>, Body)>> {
        Ok((None, response))
    }
}

async fn perform_request<'a>(
    r: Box<dyn Resource + 'a>,
    request: &'a ResourceRequest<'a>,
    body: hyper::Body,
) -> Response<Body> {
    let mut resource = r;
    let response = hyper::Response::builder();
    let mut ctx = ResourceContext::new();

    crate::flow::start(&mut *resource, &mut ctx, request, body, response)
        .await
        .unwrap_or_else(|e| match e {
            ResourceError::Halt(code) => hyper::Response::builder()
                .status(code)
                .body(Body::empty())
                .unwrap(),
            ResourceError::Err(_err) => hyper::Response::builder()
                .status(500)
                .body(Body::empty())
                .unwrap(),
        })
}

async fn not_found_fallback() -> hyper::Response<hyper::Body> {
    hyper::Response::builder()
        .status(404)
        .body(Body::empty())
        .unwrap()
}

pub type ResourceRouter<'a> =
    crate::routing::router::Router<'a, Box<dyn (Fn() -> Box<dyn Resource>) + Sync + Send + 'a>>;

async fn find_resource_and_invoke<'a>(
    router: Arc<ResourceRouter<'a>>,
    request: hyper::Request<hyper::Body>,
) -> Result<hyper::Response<hyper::Body>, Infallible> {
    let (parts, body) = request.into_parts();
    match router.find_route(parts.uri.path()) {
        Some((route_match, resource)) => {
            let res_request = ResourceRequest::new(route_match, &parts);
            Ok(perform_request(resource(), &res_request, body).await)
        }
        None => Ok(not_found_fallback().await),
    }
}

async fn invoke<'a>(
    resource: Box<dyn Resource>,
    request: hyper::Request<hyper::Body>,
) -> Result<hyper::Response<hyper::Body>, Infallible> {
    let rm = crate::routing::route::RouteMatch::empty();
    let (parts, body) = request.into_parts();
    let res_request = ResourceRequest::new(rm, &parts);
    Ok(perform_request(resource, &res_request, body).await)
}

pub fn resource_router_fn<'a>(
    router: ResourceRouter<'a>,
) -> impl FnMut(
    Request<Body>,
) -> Pin<
    Box<dyn std::future::Future<Output = Result<Response<Body>, Infallible>> + Send + 'a>,
> + 'a {
    let shared_router = Arc::new(router);

    let f = move |request: hyper::Request<Body>| {
        find_resource_and_invoke(shared_router.clone(), request).boxed()
    };

    f
}

pub fn resource_fn<'a>(
    resource: impl (Fn() -> Box<dyn Resource>) + Sync + Send + 'a,
) -> impl (FnMut(
    Request<Body>,
) -> Pin<
    Box<dyn std::future::Future<Output = Result<Response<Body>, Infallible>> + std::marker::Send>,
>) + 'a {
    let resource_factory = Mutex::new(resource);
    let f = move |request: hyper::Request<Body>| {
        invoke(resource_factory.lock().unwrap()(), request).boxed()
    };

    f
}
