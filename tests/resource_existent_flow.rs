#[macro_use]
extern crate lazy_static;

#[macro_use]
mod helpers;

use async_trait::async_trait;
use futures::future::ok;
use futures::future::FutureExt;
use http::{Method, StatusCode};
use httpdate::fmt_http_date;
use hyper::Body;
use seize::http::headers::ETag;
use seize::http::media_type::MediaType;
use seize::resource::{
    resource_fn, FutureResourceResponse, Resource, ResourceError, ResourceRequest, ResourceResponse,
};
use std::collections::{HashMap, HashSet};
use std::time::{Duration, SystemTime};

const ETAG: ETag = ETag::Weak("ABCDEF");
lazy_static! {
    static ref LAST_MODIFIED: SystemTime = SystemTime::now() - Duration::from_secs(360000);
}

struct ProcessPostResource;

#[async_trait]
impl Resource for ProcessPostResource {
    async fn etag<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<ETag<'a>>> {
        Ok((Some(ETAG), response))
    }

    async fn last_modified(
        &mut self,
        _r: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<Option<SystemTime>> {
        Ok((Some(*LAST_MODIFIED), resp))
    }

    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
        let mut map = HashMap::new();

        map.insert(
            MediaType::new("application", "json", vec![]),
            ok(Body::from("<html>Hi</html>")).boxed(),
        );
        Ok((map, response))
    }
    async fn process_post<'a>(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        body: hyper::Body,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<(MediaType<'a>, Body)>> {
        hyper::body::to_bytes(body)
            .await
            .map(|bytes| {
                (
                    Some((
                        MediaType::new("application", "json", vec![]),
                        Body::from(bytes),
                    )),
                    response,
                )
            })
            .map_err(|_e| ResourceError::Halt(StatusCode::INTERNAL_SERVER_ERROR))
    }

    async fn allowed_methods(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        r: http::response::Builder,
    ) -> ResourceResponse<HashSet<Method>> {
        Ok((
            vec![Method::POST, Method::GET, Method::DELETE]
                .into_iter()
                .collect(),
            r,
        ))
    }
}

// TODO: Check the body matches!
status_check_test! {
    request_process_post_returns_ok,
    ProcessPostResource,
    http::Request::post("/").header("accept", "application/json").body(Body::from("{\"name\":\"Bob\"}")).unwrap(),
    OK
}

status_check_test! {
    request_with_if_match_etag_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-Match", format!("{}", ETAG)).body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_if_match_wildcard_etag_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-Match", "*").body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_if_match_different_etag_returns_precondition_failed,
    ProcessPostResource,
    http::Request::get("/").header("If-Match", format!("{}", ETag::Strong("EFG"))).body(Body::empty()).unwrap(),
    PRECONDITION_FAILED
}

status_check_test! {
    request_with_if_unmodified_since_after_date_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-Unmodified-Since", fmt_http_date(*LAST_MODIFIED + Duration::from_secs(3600))).body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_if_unmodified_since_invalid_date_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-Unmodified-Since", "Crazy-Pants McGee!").body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_if_unmodified_since_before_date_returns_precondition_failed,
    ProcessPostResource,
    http::Request::get("/").header("If-Unmodified-Since", fmt_http_date(*LAST_MODIFIED - Duration::from_secs(3600))).body(Body::empty()).unwrap(),
    PRECONDITION_FAILED
}

status_check_test! {
    delete_request_with_if_none_match_same_etag_returns_precondition_failed,
    ProcessPostResource,
    http::Request::delete("/").header("If-None-Match", format!("{}", ETAG)).body(Body::empty()).unwrap(),
    PRECONDITION_FAILED
}

status_check_test! {
    get_request_with_if_none_match_same_etag_returns_precondition_failed,
    ProcessPostResource,
    http::Request::get("/").header("If-None-Match", format!("{}", ETAG)).body(Body::empty()).unwrap(),
    NOT_MODIFIED
}

status_check_test! {
    get_request_with_if_none_match_wildcard_etag_returns_not_modified,
    ProcessPostResource,
    http::Request::get("/").header("If-None-Match", format!("{}", "*")).body(Body::empty()).unwrap(),
    NOT_MODIFIED
}

status_check_test! {
    get_request_with_if_none_match_different_etag_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-None-Match", format!("{}", ETag::Strong("1234"))).body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_if_modified_since_after_last_modified_date_returns_not_modified,
    ProcessPostResource,
    http::Request::get("/").header("If-Modified-Since", fmt_http_date(*LAST_MODIFIED + Duration::from_secs(3600))).body(Body::empty()).unwrap(),
    NOT_MODIFIED
}

status_check_test! {
    request_with_if_modified_since_before_last_modified_date_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-Modified-Since", fmt_http_date(*LAST_MODIFIED - Duration::from_secs(3600))).body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_if_modified_since_in_the_future_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-Modified-Since", fmt_http_date(SystemTime::now() + Duration::from_secs(3600))).body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_if_modified_since_invalid_date_returns_ok,
    ProcessPostResource,
    http::Request::get("/").header("If-Modified-Since", "Boaty McBoatface").body(Body::empty()).unwrap(),
    OK
}
