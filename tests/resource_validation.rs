#[macro_use]
mod helpers;

use async_trait::async_trait;
use seize::resource::{resource_fn, Resource, ResourceRequest, ResourceResponse};

struct DefaultResource;

#[async_trait]
impl Resource for DefaultResource {}

struct ServiceUnavailableResource;

#[async_trait]
impl Resource for ServiceUnavailableResource {
    async fn service_available(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((false, response))
    }
}

status_check_test! {
    unavailable_resource_returns_service_unavailable_status,
    ServiceUnavailableResource,
    SERVICE_UNAVAILABLE
}

status_check_test! {
    req_with_unknown_method_returns_not_implemted_status,
    DefaultResource,
    http::Request::builder()
        .method("STAT")
        .body(hyper::Body::empty())
        .unwrap(),
    NOT_IMPLEMENTED
}

struct UriTooLongResource;

#[async_trait]
impl Resource for UriTooLongResource {
    async fn request_uri_too_long(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
        _: &'_ http::Uri,
    ) -> ResourceResponse<bool> {
        Ok((true, response))
    }
}

status_check_test! {
    resource_req_uri_too_long_returns_correct_status,
    UriTooLongResource,
    URI_TOO_LONG
}

status_check_test! {
    resource_req_unallowed_method_returns_correct_status,
    DefaultResource,
    http::Request::delete("/").body(hyper::Body::empty()).unwrap(),
    METHOD_NOT_ALLOWED
}

struct UnauthorizedResource;

#[async_trait]
impl Resource for UnauthorizedResource {
    async fn is_authorized(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
        _: Option<&'_ http::HeaderValue>,
    ) -> ResourceResponse<bool> {
        Ok((false, response))
    }
}

status_check_test! {
    request_unauthorized_returns_correct_status,
    UnauthorizedResource,
    UNAUTHORIZED
}

struct ForbiddenResource;

#[async_trait]
impl Resource for ForbiddenResource {
    async fn forbidden(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((true, response))
    }
}

status_check_test! {
    request_is_forbidden_returns_correct_status,
    ForbiddenResource,
    FORBIDDEN
}

struct UnknownContentTypeResource;

#[async_trait]
impl Resource for UnknownContentTypeResource {
    async fn known_content_type(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
        _: Option<&'_ http::HeaderValue>,
    ) -> ResourceResponse<bool> {
        Ok((false, response))
    }
}

status_check_test! {
    request_unknown_content_type_returns_correct_status,
    UnknownContentTypeResource,
    UNSUPPORTED_MEDIA_TYPE
}

struct MalformedRequestResource;

#[async_trait]
impl Resource for MalformedRequestResource {
    async fn malformed_request(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((true, response))
    }
}

status_check_test! {
    malformed_request_returns_correct_status,
    MalformedRequestResource,
    BAD_REQUEST
}

struct RequestEntityTooLargeResource;

#[async_trait]
impl Resource for RequestEntityTooLargeResource {
    async fn valid_entity_length(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
        _: Option<u64>,
    ) -> ResourceResponse<bool> {
        Ok((false, response))
    }
}

status_check_test! {
    request_too_large_returns_correct_status,
    RequestEntityTooLargeResource,
    hyper::Request::get("/").header("Content-Length", "14").body(hyper::Body::from("THIS IS A BODY")).unwrap(),
    PAYLOAD_TOO_LARGE
}
