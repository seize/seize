#[macro_use]
mod helpers;

use async_trait::async_trait;
use http::header::{HeaderName, HeaderValue};
use seize::resource::{resource_fn, Resource, ResourceRequest, ResourceResponse};

struct OptionsResource;

#[async_trait]
impl Resource for OptionsResource {
    async fn options(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Vec<(HeaderName, HeaderValue)>> {
        Ok((
            vec![(
                HeaderName::from_static("x-custom-header"),
                HeaderValue::from_static("TEST"),
            )],
            response,
        ))
    }
}

status_check_test! {
    resource_options_returned_from_options_request,
    OptionsResource,
    http::Request::options("*")
        .body(hyper::Body::empty())
        .unwrap(),
    OK,
    vec![("X-CUSTOM-HEADER", Some(HeaderValue::from_static("TEST")))]
}
