pub fn test_req() -> http::Request<hyper::Body> {
    http::Request::get("/").body(hyper::Body::empty()).unwrap()
}

macro_rules! status_check_test {
    ($n:ident, $res:ident, $req:expr, $status:ident, $headers:expr) => {
        #[test]
        pub fn $n() {
            let factory: Box<dyn Fn() -> Box<dyn Resource> + Sync + Send> =
                Box::new(|| Box::new($res));

            let resp = futures::executor::block_on(resource_fn(factory)($req)).unwrap();

            dbg!(&resp.headers().get("x-seize-trace"));
            assert_eq!(resp.status(), http::StatusCode::$status);
            for (h, v) in $headers.into_iter() {
                assert_eq!(resp.headers().get(h), v.as_ref());
            }
        }
    };
    ($n:ident, $res:ident, $req:expr, $status:ident) => {
        #[test]
        pub fn $n() {
            let factory: Box<dyn Fn() -> Box<dyn Resource> + Sync + Send> =
                Box::new(|| Box::new($res));

            let resp = futures::executor::block_on(resource_fn(factory)($req)).unwrap();

            dbg!(&resp.headers().get("x-seize-trace"));
            
            assert_eq!(resp.status(), http::StatusCode::$status);
        }
    };
    ($n:ident, $res:ident, $status:ident) => {
        status_check_test! { $n, $res, helpers::test_req(), $status}
    };
}
