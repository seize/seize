#[macro_use]
mod helpers;

use async_trait::async_trait;
use futures::future::ok;
use futures::future::FutureExt;
use http::HeaderValue;
use hyper::Body;
use seize::http::media_type::MediaType;
use seize::language::LanguageTag;
use seize::resource::{
    resource_fn, FutureResourceResponse, Resource, ResourceRequest, ResourceResponse,
};
use std::collections::HashMap;
use std::convert::TryFrom;

struct HtmlResource;

#[async_trait]
impl Resource for HtmlResource {
    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
        let mut map = HashMap::new();

        map.insert(
            MediaType::new("text", "html", vec![]),
            ok(Body::from("<html>Hi</html>")).boxed(),
        );
        Ok((map, response))
    }
}

status_check_test! {
    request_doesnt_accept_available_types_returns_proper_status_code,
    HtmlResource,
    http::Request::get("/").header("accept", "application/json").body(Body::empty()).unwrap(),
    NOT_ACCEPTABLE
}

status_check_test! {
    request_without_accept_returns_proper_status_code,
    HtmlResource,
    http::Request::get("/").body(Body::empty()).unwrap(),
    OK,
    vec![("content-type", Some(HeaderValue::from_static("text/html")))]
}

struct HtmlLangResource;

#[async_trait]
impl Resource for HtmlLangResource {
    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
        let mut map = HashMap::new();

        map.insert(
            MediaType::new("text", "html", vec![]),
            ok(Body::from("<html>Hi</html>")).boxed(),
        );
        Ok((map, response))
    }

    async fn languages_provided<'a>(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<Vec<LanguageTag<'a>>> {
        Ok((
            vec![LanguageTag::try_from(b"en-GB" as &[u8]).unwrap()],
            resp,
        ))
    }
}

status_check_test! {
    request_with_accept_language_provided_returns_proper_status_code,
    HtmlLangResource,
    http::Request::get("/").header("Accept-Language", "en-GB").body(Body::empty()).unwrap(),
    OK,
    vec![("content-language", Some(HeaderValue::from_static("en-GB")))]
}

status_check_test! {
    request_with_accept_language_not_provided_still_succeeds,
    HtmlLangResource,
    http::Request::get("/").header("Accept-Language", "ch").body(Body::empty()).unwrap(),
    OK,
    vec![("content-language", None)]
}

struct HtmlCharsetResource;

#[async_trait]
impl Resource for HtmlCharsetResource {
    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
        let mut map = HashMap::new();

        map.insert(
            MediaType::new("text", "html", vec![]),
            ok(Body::from("<html>Hi</html>")).boxed(),
        );
        Ok((map, response))
    }

    async fn charsets_provided<'a>(
        &mut self,
        _: &'_ ResourceRequest<'_>,
        resp: http::response::Builder,
    ) -> ResourceResponse<Vec<&'a str>> {
        Ok((vec!["utf-8"], resp))
    }
}

status_check_test! {
    request_with_accept_charset_provided_returns_proper_status_code,
    HtmlCharsetResource,
    http::Request::get("/").header("Accept-Charset", "utf-8").body(Body::empty()).unwrap(),
    OK
}

status_check_test! {
    request_with_accept_charset_not_provided_still_succeeds,
    HtmlCharsetResource,
    http::Request::get("/").header("Accept-Charset", "jcp").body(Body::empty()).unwrap(),
    OK
}
