extern crate async_trait;
extern crate hyper;
extern crate seize;

use async_trait::async_trait;
use futures::future::FutureExt;
use hyper::header::{HeaderName, HeaderValue};
use hyper::service::{make_service_fn, service_fn};
use hyper::Method;
use hyper::{Body, Server, StatusCode};
use seize::http::headers::ETag;
use seize::http::media_type::MediaType;
use seize::language::LanguageTag;
use seize::resource::ResourceError;
use seize::resource::ResourceRouter;
use seize::resource::{
    resource_router_fn, AcceptStatus, FutureResourceResponse, ResourceRequest, ResourceResponse,
};
use std::boxed::Box;
use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::TryFrom;
use std::error::Error;

struct MyResource {}

impl MyResource {
    fn new() -> MyResource {
        MyResource {}
    }

    async fn to_html(&mut self) -> Result<Body, ResourceError> {
        Ok(Body::from("<html>Hi</html>"))
    }
}

fn get_content_type(headers: &http::HeaderMap) -> MediaType<'_> {
    headers
        .get(hyper::header::CONTENT_TYPE)
        .ok_or(ResourceError::Halt(StatusCode::BAD_REQUEST))
        .and_then(|h| {
            MediaType::try_from(h.as_bytes())
                .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))
        })
        .unwrap_or(MediaType::new("octet", "stream", vec![]))
}

#[async_trait]
impl seize::resource::Resource for MyResource {
    async fn service_available(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((true, response))
    }

    async fn allowed_methods(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashSet<Method>> {
        Ok((
            [
                Method::OPTIONS,
                Method::GET,
                Method::HEAD,
                Method::POST,
                Method::PUT,
            ]
            .iter()
            .cloned()
            .collect(),
            response,
        ))
    }

    async fn options(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Vec<(HeaderName, HeaderValue)>> {
        Ok((
            vec![(
                HeaderName::from_static("allow-origin"),
                HeaderValue::from_static("*"),
            )],
            response,
        ))
    }

    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
        let mut map = HashMap::new();

        map.insert(
            MediaType::new("text", "html", vec![]),
            self.to_html().boxed(),
        );

        Ok((map, response))
    }

    async fn content_type_accepter<'s, 'a>(
        &'s mut self,
        request: &'s ResourceRequest<'s>,
        _body: hyper::Body,
        response: http::response::Builder,
        _uri: &'s http::Uri,
    ) -> ResourceResponse<AcceptStatus<'a>> {
        let content_type = get_content_type(request.headers());
        Ok((
            match (content_type.type_(), content_type.subtype()) {
                ("application", "json") => AcceptStatus::Accepted(Some((
                    MediaType::new("text", "plain", vec![]),
                    Body::from("ABC"),
                ))),
                _ => AcceptStatus::Unsupported,
            },
            response,
        ))
    }

    async fn languages_provided<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Vec<LanguageTag<'a>>> {
        Ok((
            vec![
                LanguageTag::try_from(b"en-GB" as &[u8]).unwrap(),
                LanguageTag::try_from(b"de" as &[u8]).unwrap(),
            ],
            response,
        ))
    }

    async fn charsets_provided<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Vec<&'a str>> {
        Ok((vec!["UTF-8"], response))
    }

    async fn etag<'a>(
        &'_ mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<ETag<'a>>> {
        Ok((Some(ETag::Weak("ABCDEF")), response))
    }

    async fn process_post<'a>(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        _body: hyper::Body,
        response: http::response::Builder,
    ) -> ResourceResponse<Option<(MediaType<'a>, Body)>> {
        Ok((
            Some((
                MediaType::new("text", "plain", vec![]),
                Body::from("A Wild response appears"),
            )),
            response,
        ))
    }
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let addr = ([127, 0, 0, 1], 3000).into();

    let new_svc = make_service_fn(|_| {
        async {
            let router = ResourceRouter::new()
                .route()
                .segment("")
                .to(Box::new(|| Box::new(MyResource::new())));

            Ok::<_, hyper::Error>(service_fn(resource_router_fn(router)))
        }
    });

    let server = Server::bind(&addr).serve(new_svc);

    println!("Listening on http://{}", addr);
    server.await?;

    Ok(())
}
