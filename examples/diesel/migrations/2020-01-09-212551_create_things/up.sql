-- Your SQL goes here
CREATE TABLE things (
  id SERIAL PRIMARY KEY,
  active BOOLEAN NOT NULL DEFAULT 't'
)
