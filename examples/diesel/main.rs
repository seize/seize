extern crate async_trait;
#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate hyper;
extern crate seize;
extern crate serde;
extern crate serde_json;
extern crate tokio_diesel;

use async_trait::async_trait;
use diesel::{
    pg::PgConnection,
    prelude::*,
    r2d2::{ConnectionManager, Pool},
};
use dotenv::dotenv;
use futures::future::FutureExt;
use hyper::service::{make_service_fn, service_fn};
use hyper::Method;
use hyper::{Body, Server, StatusCode};
use seize::http::media_type::MediaType;
use seize::resource::ResourceError;
use seize::resource::ResourceRouter;
use seize::resource::{
    resource_router_fn, AcceptStatus, FutureResourceResponse, ResourceRequest, ResourceResponse,
};
use seize::routing::route::RouteMatchItem;
use serde::{Deserialize, Serialize};

use std::boxed::Box;
use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::TryFrom;
use std::env;
use std::error::Error;
use std::sync::Arc;
use tokio_diesel::*;

type DbPool = Pool<ConnectionManager<PgConnection>>;

pub fn create_db_pool() -> Result<DbPool, Box<dyn Error + Sync + Send>> {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let cm = ConnectionManager::new(database_url);
    Ok(Pool::builder().build(cm)?)
}

table! {
    things (id) {
        id -> Integer,
        active -> Bool,
    }
}

#[derive(Queryable, Debug, Serialize, Deserialize)]
pub struct Thing {
    pub id: i32,
    pub active: bool,
}

impl From<AsyncError> for LoadError {
    fn from(inner: AsyncError) -> Self {
        LoadError { inner }
    }
}

impl From<LoadError> for ResourceError {
    fn from(_: LoadError) -> ResourceError {
        ResourceError::Halt(hyper::StatusCode::INTERNAL_SERVER_ERROR)
    }
}

#[derive(Debug)]
struct LoadError {
    inner: AsyncError,
}

impl std::fmt::Display for LoadError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.inner)
    }
}

impl std::error::Error for LoadError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(&self.inner)
    }
}

struct ThingCollectionResource {
    db: Arc<DbPool>,
    things: Option<Vec<Thing>>,
}

impl ThingCollectionResource {
    fn new(db: Arc<DbPool>) -> Self {
        ThingCollectionResource { things: None, db }
    }

    async fn things(&mut self) -> Result<&Vec<Thing>, LoadError> {
        match self.things {
            Some(ref things) => Ok(things),
            None => {
                use crate::things::dsl::*;

                let coll = things
                    .filter(active.eq(true))
                    .load_async::<Thing>(&self.db)
                    .await?;

                self.things = Some(coll);

                Ok(self.things.as_ref().unwrap())
            }
        }
    }

    async fn to_json(&mut self) -> Result<Body, ResourceError> {
        match serde_json::to_vec(&self.things().await?) {
            Ok(b) => Ok(Body::from(b)),
            Err(_) => Err(ResourceError::Halt(StatusCode::INTERNAL_SERVER_ERROR)),
        }
    }
}

#[async_trait]
impl seize::resource::Resource for ThingCollectionResource {
    async fn resource_exists(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<bool> {
        Ok((self.things().await.is_ok(), response))
    }

    async fn allowed_methods(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashSet<Method>> {
        Ok(([Method::GET].iter().cloned().collect(), response))
    }

    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
        let mut map = HashMap::new();

        map.insert(
            MediaType::new("application", "json", vec![]),
            self.to_json().boxed(),
        );

        Ok((map, response))
    }

    async fn content_type_accepter<'s, 'a>(
        &'s mut self,
        request: &'s ResourceRequest<'s>,
        _body: hyper::Body,
        response: http::response::Builder,
        _uri: &'s http::Uri,
    ) -> ResourceResponse<AcceptStatus<'a>> {
        let content_type = get_content_type(request.headers());
        Ok((
            match (content_type.type_(), content_type.subtype()) {
                ("application", "json") => AcceptStatus::Accepted(Some((
                    MediaType::new("text", "plain", vec![]),
                    Body::from("ABC"),
                ))),
                _ => AcceptStatus::Unsupported,
            },
            response,
        ))
    }
}

struct ThingResource {
    db: Arc<DbPool>,
    thing: Option<Thing>,
}

impl ThingResource {
    fn new(db: Arc<DbPool>) -> Self {
        ThingResource { thing: None, db }
    }

    async fn get_thing(&self, thing_id: i32) -> Result<Thing, Box<dyn Error>> {
        use crate::things::dsl::*;

        let t = things
            .find(thing_id)
            .get_result_async::<Thing>(&self.db)
            .await?;

        Ok(t)
    }

    async fn to_json(&mut self) -> Result<Body, ResourceError> {
        match serde_json::to_vec(&self.thing) {
            Ok(b) => Ok(Body::from(b)),
            Err(_) => Err(ResourceError::Halt(StatusCode::INTERNAL_SERVER_ERROR)),
        }
    }
}

fn get_content_type(headers: &http::HeaderMap) -> MediaType<'_> {
    headers
        .get(hyper::header::CONTENT_TYPE)
        .ok_or(ResourceError::Halt(StatusCode::BAD_REQUEST))
        .and_then(|h| {
            MediaType::try_from(h.as_bytes())
                .map_err(|_| ResourceError::Halt(StatusCode::BAD_REQUEST))
        })
        .unwrap_or(MediaType::new("octet", "stream", vec![]))
}

#[async_trait]
impl seize::resource::Resource for ThingResource {
    async fn resource_exists(
        &mut self,
        request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<bool> {
        let id_opt = request.route().get("id").and_then(|i| match i {
            RouteMatchItem::Value(id_str) => id_str.parse().ok(),
            _ => None,
        });

        if let Some(id) = id_opt {
            self.thing = self.get_thing(id).await.ok(); // TODO: Errors raised and turned into 500 codes?
        }
        Ok((self.thing.is_some(), response))
    }

    async fn allowed_methods(
        &mut self,
        _request: &'_ ResourceRequest<'_>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashSet<Method>> {
        Ok(([Method::GET].iter().cloned().collect(), response))
    }

    async fn content_types_provided<'s, 'a>(
        &'s mut self,
        _request: &'s ResourceRequest<'s>,
        response: http::response::Builder,
    ) -> ResourceResponse<HashMap<MediaType<'a>, FutureResourceResponse<'s, Body>>> {
        let mut map = HashMap::new();

        map.insert(
            MediaType::new("application", "json", vec![]),
            self.to_json().boxed(),
        );

        Ok((map, response))
    }

    async fn content_type_accepter<'s, 'a>(
        &'s mut self,
        request: &'s ResourceRequest<'s>,
        _body: hyper::Body,
        response: http::response::Builder,
        _uri: &'s http::Uri,
    ) -> ResourceResponse<AcceptStatus<'a>> {
        let content_type = get_content_type(request.headers());
        Ok((
            match (content_type.type_(), content_type.subtype()) {
                ("application", "json") => AcceptStatus::Accepted(Some((
                    MediaType::new("text", "plain", vec![]),
                    Body::from("ABC"),
                ))),
                _ => AcceptStatus::Unsupported,
            },
            response,
        ))
    }
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let addr = ([127, 0, 0, 1], 3000).into();

    let d = Arc::new(create_db_pool()?);
    let new_svc = make_service_fn(move |_| {
        let db_copy = d.clone();

        let coll_db_copy = d.clone();
        async move {
            let router = ResourceRouter::new()
                .route()
                .segment("thing")
                .capture_segment("id")
                .to(Box::new(move || {
                    Box::new(ThingResource::new(db_copy.clone()))
                }))
                .route()
                .segment("thing")
                .to(Box::new(move || {
                    Box::new(ThingCollectionResource::new(coll_db_copy.clone()))
                }));
            Ok::<_, hyper::Error>(service_fn(resource_router_fn(router)))
        }
    });

    let server = Server::bind(&addr).serve(new_svc);

    println!("Listening on http://{}", addr);
    server.await?;

    Ok(())
}
